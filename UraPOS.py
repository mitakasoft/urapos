from tkinter import *
from tkinter import messagebox
import sys
import os

#Permanent Measurements
#Cells
cellX = 25 
cellY = 15 
cellWidth = 63
cellHeight = 59

#Screen Dimensions
screenWidth = 1580
screenHeight = 878

#Top Menu Bar Dimensions
TopBarWidth = screenWidth
TopBarHeight = cellHeight
TopBarPosX = 0
TopBarPosY = 0

#Bottom Menu Bar Dimensions
BottomBarWidth = screenWidth
BottomBarHeight = cellHeight
TopBarPosX = 0
TopBarPosY = screenHeight - cellHeight

#Universal Button Dimensions
buttonWidth = cellWidth * 2
buttonHeight = cellHeight * 2

#Maths
factorArray = [0]
integerCount = 0

##Window Creation
root = Tk()
root.title("POS System")
root.resizable(width=False, height=False)
root.geometry(str(screenWidth)+"x"+str(screenHeight))

##MenuBar
topMenu = Frame(root, bg="blue")
topMenu.grid(column=1, row=1, columnspan = 25, rowspan=1)

##DisplayScreen
displayScreen = Frame(root, bg="red")
displayScreen.grid(column=1, row= 2, columnspan = 9, rowspan = 4)

##DisplayText on the calcScreen
def makeInteger(buttonInt):
	calcScreen.insert(END, str(buttonInt))

def clearEntry():
	global factorArray
	calcScreen.delete(0, END)
	factorArray=[]

def addFactor(operation):
	global factorArray
	global integerCount
	integerCount += 1
	try:
		getInteger = int(calcScreen.get())
		if operation == "+":
			factorArray.append(getInteger)
		if operation == "-":
			if integerCount <= 0 or integerCount == 1:
				factorArray.append(getInteger)
			else:
				factorArray.append(-getInteger)
	except ValueError:
		getTotal()

	calcScreen.delete(0, END)

	

	print(str(factorArray))

subTotal = 0
def getTotal():
	global factorArray
	global subTotal
	try:
		subTotal = int(calcScreen.get())
		for i in factorArray:
			subTotal += i
	except ValueError:
		for i in factorArray:
			subTotal += i

	print(subTotal)

def getTaxTotal():
	global subTotal
	global factorArray
	calcScreen.delete(0, END)
	grandTotal = 0
	if subTotal == 0:
		messagebox.showinfo("Error", "No prices listed.")
	else:
		taxtotal = subTotal * .08
		grandTotal = subTotal + taxtotal

	factorArray = [grandTotal]
	print("Subtotal: ¥" + str(int(subTotal)))
	print("Plus Tax: ¥" + str(int(taxtotal)))
	print("-----------------------------")
	print("Total Due: ¥" + str(int(grandTotal)))







##Screen Setup
calcScreen = Entry(root, font="none 100 bold", fg="red", justify=RIGHT, width=10)
calcScreen.grid(column=1, row=2, columnspan=10, rowspan=5, sticky=W)
#calcScreen.config(state=DISABLED)

##CalcSetup
calcInterFrame = Frame(root)
calcInterFrame.grid(column=1, row=7, columnspan=10, rowspan=9, sticky=W)
numButton = [ Button(calcInterFrame, text="0", bg="silver", font="none 50 bold", command=lambda: makeInteger(0)),
Button(calcInterFrame, text="1", bg="silver", font="none 50 bold", command=lambda: makeInteger(1)),
Button(calcInterFrame, text="2", bg="silver", font="none 50 bold", command=lambda: makeInteger(2)),
Button(calcInterFrame, text="3", bg="silver", font="none 50 bold", command=lambda: makeInteger(3)),
Button(calcInterFrame, text="4", bg="silver", font="none 50 bold", command=lambda: makeInteger(4)),
Button(calcInterFrame, text="5", bg="silver", font="none 50 bold", command=lambda: makeInteger(5)),
Button(calcInterFrame, text="6", bg="silver", font="none 50 bold", command=lambda: makeInteger(6)),
Button(calcInterFrame, text="7", bg="silver", font="none 50 bold", command=lambda: makeInteger(7)),
Button(calcInterFrame, text="8", bg="silver", font="none 50 bold", command=lambda: makeInteger(8)),
Button(calcInterFrame, text="9", bg="silver", font="none 50 bold", command=lambda: makeInteger(9))
]

numButton[1].grid(column=1, row=7, columnspan=2, rowspan=2)
numButton[2].grid(column=3, row=7, columnspan=2, rowspan=2)
numButton[3].grid(column=5, row=7, columnspan=2, rowspan=2)

numButton[4].grid(column=1, row=9, columnspan=2, rowspan=2)
numButton[5].grid(column=3, row=9, columnspan=2, rowspan=2)
numButton[6].grid(column=5, row=9, columnspan=2, rowspan=2)

numButton[7].grid(column=1, row=11, columnspan=2, rowspan=2)
numButton[8].grid(column=3, row=11, columnspan=2, rowspan=2)
numButton[9].grid(column=5, row=11, columnspan=2, rowspan=2)

numButton[0].grid(column=3, row=13, columnspan=2, rowspan=2)

clearButton = Button(calcInterFrame, text="C", bg="silver", font="none 48 bold", command=clearEntry)
clearButton.grid(column=1, row=13, columnspan=2, rowspan=2)

pointButton = Button(calcInterFrame, text=".", bg="silver", font="none 50 bold", command=lambda: makeInteger("."))
pointButton.grid(column=5, row=13, columnspan=2, rowspan=2)

plusButton = Button(calcInterFrame, text="+", bg="silver", font="none 50 bold", command=lambda: addFactor("+"))
plusButton.grid(column=7, row=9, columnspan=2, rowspan=2)

minusButton = Button(calcInterFrame, text="+/-", bg="silver", font="none 50 bold", command=lambda: addFactor("-"))
minusButton.grid(column=7, row=7, columnspan=2, rowspan=2)

equalButton = Button(calcInterFrame, text="=", bg="green", font="none 50 bold", command=getTotal)
equalButton.grid(column=7, row=13, columnspan=2, rowspan=2)

taxButton = Button(calcInterFrame, text="Total\nw/Tax", bg="silver", font="none 20 bold", command=getTaxTotal)
taxButton.grid(column=7, row=11, columnspan=2, rowspan=2)

root.mainloop()