from tkinter import *
import random
import sys
import os
from tkinter import messagebox
import sqlite3 ##REMOVE FOR INTEGRATION##

windowType = "Create Employee"

bgColor = '#253340'
borderGray = '#757272'
mainFont = "Helvetica 18"

rootWindow = Tk()
rootWindow.geometry("640x480")
rootWindow.resizable(width=False, height=False)
rootWindow.title(windowType)

bg = PhotoImage(file="formtools/background.gif")
bgImage = Label(rootWindow, image=bg).place(x=-3, y=-3)

##REMOVE FOR INTEGRATION##
conn = sqlite3.connect('TestDB.db')
c = conn.cursor()
c2= conn.cursor()

def killMe():
	rootWindow.destroy()

def destroyMe(event):
	killMe()

rootWindow.bind("<Escape>", destroyMe)

def resetEntries():
	if messagebox.askyesno("Confirm", "Are you sure you want to reset the form?"):
		numberEntry.delete(0,END)
		lastEntry.delete(0,END)
		firstEntry.delete(0,END)
		YOBEntry.delete(0,END)
		MOBEntry.delete(0,END)
		DOBEntry.delete(0,END)
		countryEntry.delete(0,END)
		phoneEntry.delete(0,END)
		visaEntry.delete(0,END)
		posEntry.delete(0,END)
		wageEntry.delete(0,END)
		statusEntry.delete(0,END)

def checkNumber():
	try:
		verify = numberEntry.get()
		if int(verify) < 999 or int(verify) > 9999:
			messagebox.showerror("Invalid Entry", "Numbers cannot be less than 1000 or greater than 9999, and must contain 4 digits.")
		else:
			checkNumber = c.execute("SELECT Count(*) FROM EmpTable WHERE EmpID = " +str(verify)).fetchone()[0]
			if checkNumber >= 1:
				messagebox.showerror("Conflict", str(verify)+" already exists in the database.  Please issue a different number or randomize.")
			else:
				messagebox.showwarning("It's Good", str(verify)+" is good to use.")
	except ValueError:
		messagebox.showerror("Nothing Entered", "You must enter a number in this field.")

def getRandom():
	numberEntry.delete(0,END)
	randEntry = random.randrange(1000, 9999)
	verify = c.execute("SELECT Count(*) FROM EmpTable WHERE EmpID = " +str(randEntry)).fetchone()[0]
	while verify >= 1:
		randEntry = random.randrange(1000, 9999)
		reverify = c.execute("SELECT COUNT (*) FROM EmpTable WHERE EmpID = " + str(randEntry)).fetchone()[0]
	numberEntry.insert(0, str(randEntry))

def registerPress():
	try:
		checkNumber()
		checkCountry()
	except ValueError:
		messagebox.showerror("Incomplete Input", "There are blank fields in the form.  Make sure all information is completely filled in before submission.")

def completeRegistry():
	try:
		ID = numberEntry.get()
		lastName = lastEntry.get()
		firstName = firstEntry.get()
		YOB = int(YOBEntry.get())
		MOB = int(MOBEntry.get())
		if MOB < 1 or MOB > 12:
			messagebox.showerror("Calendar Error", "There are only 12 months in a year.  Please enter 1 through 12 to indicate the month.")
		else:
			DOB = int(DOBEntry.get())
			if DOB < 1 or DOB > 32:
				messagebox.showerror("Calendar Error", "Months with 30 days: April (4), June (6), September (9), November (11).\nIrregular Month: February (2)\nAll other months: 31 Days.\nNo month has "+str(DOB)+" days.")
			else:
				phoneNumber = phoneEntry.get()
				country = countryEntry.get()
				theVisa = visaEntry.get()
				position = posEntry.get()
				pay = wageEntry.get()
				empStatus = statusEntry.get()

				if messagebox.askyesno("Ready to Register", "Would you like to register " + lastName+", "+firstName+"?"):
					c.execute("INSERT INTO EmpTable(EmpID, EmpLst, EmpFst, EmpYOB, EmpMOB, EmpDOB, EmpPho, EmpCou, EmpVsa, EmpPos, EmpPay, EmpSts) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
						(ID, lastName, firstName, YOB, MOB, DOB, phoneNumber, country, theVisa, position, pay, empStatus))
					conn.commit()
					messagebox.showinfo("Entry Successful", lastName+", "+firstName+"'s registry was completed successfully.")
				else:
					messagebox.showwarning("Aborted", "Submission cancelled.")
	except ValueError:
		messagebox.showerror("Error", "Some data was invalid or incomplete.\nPlease verify and resubmit the form.")



def checkCountry():
	country = countryEntry.get()
	if country != "JAPAN" or country!= "Japan" or country != "japan" or country != "日本":
		theVisa = visaEntry.get()
		if theVisa != "KOKUSEKI" or theVisa != "kokuseki" or theVisa != "Kokuseki" or theVisa != "日本国籍" or theVisa != "国籍":
			if messagebox.askyesno("Visa Warning", "Have you verified the employee's right to work in Japan?"):
				completeRegistry()
	else:
		if messagebox.askyesno("Verify Japanese", "Have you confirmed the employee's Japanese citizenship?"):
			completeRegistry()




##Window Arrangement - to be used multiple times

cancelImage = PhotoImage(file="formtools/cancelButton.gif")
checkImage = PhotoImage(file="formtools/checkButton.gif")
randomImage = PhotoImage(file="formtools/randomButton.gif")
regImage = PhotoImage(file="formtools/registerButton.gif")
resetImage = PhotoImage(file="formtools/resetButton.gif")

titleLabel = Label(rootWindow, font=mainFont, bg=bgColor, fg="white")
titleLabel.place(x=125, y=36, anchor=CENTER)
titleLabel.config(text=windowType)

numberLabel = Label(rootWindow, text="Number", font=mainFont, bg=bgColor, fg="white") .place(x=102, y=80, anchor=CENTER)
warningLabel = Label(rootWindow, text='For Japanese Nationals, enter "JAPAN" for country\nand "KOKUSEKI" for Visa', font=mainFont, bg=bgColor, fg="red", justify=CENTER)
warningLabel.place(x=319, y=442, anchor=CENTER)

lastLabel = Label(rootWindow, text="Last", font=mainFont, bg=bgColor, fg="white") .place(x=110, y=131, anchor=CENTER)
firstLabel = Label(rootWindow, text="First", font=mainFont, bg=bgColor, fg="white") .place(x=368, y=131, anchor=CENTER)

DOBLabel = Label(rootWindow, text="Date of Birth", font=mainFont, bg=bgColor, fg="white") .place(x=142, y=163, anchor=CENTER)
homeLabel = Label(rootWindow, text="Home Country", font=mainFont, bg=bgColor, fg="white") .place(x=410, y=163, anchor=CENTER)

yearLabel = Label(rootWindow, text="Year", font=mainFont, bg=bgColor, fg="gray").place(x=110, y=216, anchor=CENTER)
monthLabel = Label(rootWindow, text="Month", font=mainFont, bg=bgColor, fg="gray") .place(x=204, y=216, anchor=CENTER)
dayLabel = Label(rootWindow, text="Day", font=mainFont, bg=bgColor, fg="gray").place(x=288, y=216, anchor=CENTER)
visaLabel = Label(rootWindow, text="Visa Status", font=mainFont, bg=bgColor, fg="white") .place(x=398, y=216, anchor=CENTER)

phoneLabel = Label(rootWindow, text="Phone", font=mainFont, bg=bgColor, fg="white").place(x=118, y=245, anchor=CENTER)
positionLabel = Label(rootWindow, text="Position", font=mainFont, bg=bgColor, fg="white").place(x=121, y=289, anchor=CENTER)
wageLabel = Label(rootWindow, text="Hourly Wage", font=mainFont, bg=bgColor, fg="white").place(x=140, y=327, anchor=CENTER)
statusLabel = Label(rootWindow, text="Employee Status", font=mainFont, bg=bgColor, fg="white") .place(x=453, y=293, anchor=CENTER)

cancelButton = Button(rootWindow, image=cancelImage, command=killMe).place(x=516, y=376, anchor=CENTER)
registerButton = Button(rootWindow, image=regImage, command=registerPress).place(x=403, y=376, anchor=CENTER)
resetButton = Button(rootWindow, image=resetImage, command=resetEntries).place(x=121, y=376, anchor=CENTER)
checkButton = Button(rootWindow, image=checkImage, command=checkNumber).place(x=293, y=79, anchor=CENTER)
randomButton = Button(rootWindow, image=randomImage, command=getRandom).place(x=374, y=79, anchor=CENTER)

numberEntry = Entry(rootWindow, bg=bgColor, fg="white", width=8, font=mainFont, justify=CENTER) 
numberEntry.place(x=197, y=79, anchor=CENTER)

lastEntry = Entry(rootWindow, bg=bgColor, fg="white", width=15, font=mainFont)
lastEntry.place(x=229, y=128, anchor=CENTER)

firstEntry = Entry(rootWindow, bg=bgColor, fg="white", width=14, font=mainFont)
firstEntry.place(x=477, y=128, anchor=CENTER)

YOBEntry = Entry(rootWindow, bg=bgColor, fg="white", width=6, font=mainFont, justify=CENTER)
YOBEntry.place(x=127, y=187, anchor=CENTER)

MOBEntry = Entry(rootWindow, bg=bgColor, fg="white", width=5, font=mainFont, justify=CENTER)
MOBEntry.place(x=213, y=187, anchor=CENTER)

DOBEntry = Entry(rootWindow, bg=bgColor, fg="white", width=5, font=mainFont, justify=CENTER)
DOBEntry.place(x=293, y=187, anchor=CENTER)

countryEntry = Entry(rootWindow, bg=bgColor, fg="white", width=18, font=mainFont)
countryEntry.place(x=454, y=187, anchor=CENTER)

visaEntry = Entry(rootWindow, bg=bgColor, fg="white", width=18, font=mainFont)
visaEntry.place(x=454, y=243, anchor=CENTER)

statusEntry = Entry(rootWindow, bg=bgColor, fg="white", width=18, font=mainFont, justify=CENTER)
statusEntry.place(x=454, y=326, anchor=CENTER)

phoneEntry = Entry(rootWindow, bg=bgColor, fg="white", width=15, font=mainFont)
phoneEntry.place(x=236, y=243, anchor=CENTER)

posEntry = Entry(rootWindow, bg=bgColor, fg="white", width=14, font=mainFont)
posEntry.place(x=242, y=287, anchor=CENTER)

wageEntry = Entry(rootWindow, bg=bgColor, fg="white", width=10, font=mainFont)
wageEntry.place(x=257, y=326, anchor=CENTER)





rootWindow.mainloop()