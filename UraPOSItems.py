import sys
import os
import sqlite3
import json

##Taking 2 approaches
## 1) Defining items and prices with a JSON
## 1A) Making them user customizable
## 2) Creating an SQLite DB
## 2A) Making the values line up with a customizable JSON

inventory = { "Item 1": 0,
			  "Item 2": 0,
			  "Item 3": 0,
			  "Item 4": 0,
			  "Item 5": 0,
			  "Item 6": 0,
			  "Item 7": 0,
			  "Item 8": 0,
			  "Item 9": 0,
			  "Item 10": 0,
			  "Item 11": 0,
			  "Item 12": 0,
			  "Item 13": 0,
			  "Item 14": 0,
			  "Item 15": 0,
			  "Item 16": 0,
			  "Item 17": 0,
			  "Item 18": 0 
			  }