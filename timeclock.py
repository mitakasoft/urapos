from tkinter import *
from tkinter import messagebox
import datetime
import time
import sqlite3
import sys
import os

##Time Clock main Window
##Needs to have access to everything in order to function
##To work on all systems (Engineered on macOS, runnable on Windows, Linux)
##Built for TOUCH
##TODO AFTER: Recompile for iOS (NO Android)

macSize = "257x488"
winSize = "300x500"

root = None

root = Tk()
root.title("Time Clock")
root.geometry(macSize)
root.resizable(height=False, width=False)

bgImg = PhotoImage(file="metalBG.gif")
bgLabel = Label(root, image=bgImg)
bgLabel.place(x=0, y=0, relwidth=1, relheight=1)

hasDefaultSkin = True

##Photo Array
## Keypad
photo = [PhotoImage(file="0button.gif"),
PhotoImage(file="1button.gif"),
PhotoImage(file="2button.gif"),
PhotoImage(file="3button.gif"),
PhotoImage(file="4button.gif"),
PhotoImage(file="5button.gif"),
PhotoImage(file="6button.gif"),
PhotoImage(file="7button.gif"),
PhotoImage(file="8button.gif"),
PhotoImage(file="9button.gif"),
PhotoImage(file="cancelbutton.gif"), 
PhotoImage(file="inbutton.gif"), 
PhotoImage(file="outbutton.gif"), 
PhotoImage(file="mngrbutton.gif")] 

## Connect to the db created in the Employee Editor
conn = sqlite3.connect('TestDB.db')
c = conn.cursor()

c.execute("CREATE TABLE IF NOT EXISTS TimeClock(ClockDate NUMERIC, EmpID INTEGER, ClockIN NUMERIC, ClockOUT NUMERIC, isClockedIn NUMERIC)")

##Query Key
queryKey = 0

## App Massacure
def killMe():
	messagebox.showinfo("Death", "Program will end.\nPress OK to FINISH HIM!")
	exit()

## clear the display screen
def clearEntry():
	global queryKey
	numberEntry.delete(0,END)
	queryKey=0


## Insert numbers into the display screen
def typeNumber(numberToType):
	numberEntry.insert(END, str(numberToType))


def clockIn():
	global queryKey
	clockInTime = datetime.datetime.now()
	try:
		queryKey = int(numberEntry.get())
		## Entry syntax check
		if queryKey < 1000 or queryKey > 9999:
			messagebox.showinfo("Invalid Entry", "Employee numbers are 4 digit.\nNo less than 1000.\nNo greater than 9999.\nPress OK to try again.")
		else:
			c.execute("SELECT EmpID FROM EmpTable WHERE EmpID = " +str(queryKey))
			data = c.fetchone()
			if data == None:
				messagebox.showerror("Invalid Entry", "There is no employee number " + str(queryKey)+". \nPlease recheck the number or register employee.")
				clearEntry()
			else:
				c.execute("SELECT EmpID FROM TimeClock WHERE EmpID = "+str(queryKey) + " AND isClockedIn = 1")
				checkData = c.fetchone()
				if checkData != None:
					messagebox.showwarning("Conflict", "Employee number "+str(queryKey)+" is already on the clock.\nPlease contact your manager for further details.")
					clearEntry()
				else:
					c.execute("INSERT INTO TimeClock(ClockDate, EmpID, ClockIN, ClockOut, isClockedIn) VALUES (?, ?, ?, ?, ?)", (clockInTime.strftime("%Y"+"%m"+"%d"), queryKey, clockInTime.strftime("%H"+"%M"), None, 1))
					messagebox.showinfo("Welcome", "Employee Number "+str(queryKey)+ " successfully clocked IN at " + str(clockInTime.strftime("%H" + ":" + "%M")))
					conn.commit()

	except ValueError:
		messagebox.showinfo("Empty Set", "Must enter a number to use these functions.")

	clearEntry()



## Placeholder for Clock-Out Function
def clockOut():
	global queryKey
	try:
		queryKey = int(numberEntry.get())
		clockOutTime = datetime.datetime.now()
		## Entry syntax check
		if queryKey < 1000 or queryKey > 9999:
			messagebox.showinfo("Invalid Entry", "Employee numbers are 4 digit.\nNo less than 1000.\nNo greater than 9999.\nPress OK to try again.")
		else:
			c.execute("SELECT EmpID FROM TimeClock WHERE EmpID = " +str(queryKey) + " AND isClockedIn = 1")
			data = c.fetchone()
			if data == None:
				messagebox.showerror("Invalid Entry", "There is no employee number " + str(queryKey) + "\nOR\nEmployee number " +str(queryKey) + " is not clocked in.")
				clearEntry()
			else:
				c.execute("UPDATE TimeClock SET ClockOut = " +clockOutTime.strftime("%H"+"%M")+ " WHERE EmpID = " +str(queryKey))
				c.execute("UPDATE TimeClock SET isClockedIn = 0 WHERE EmpID = " +str(queryKey))
				messagebox.showinfo("Have a Nice Day", "Employee Number "+str(queryKey)+ " successfully clocked OUT at " + str(clockOutTime.strftime("%H" + ":" + "%M")))
				conn.commit()
	except ValueError:
		messagebox.showinfo("Empty Set", "Must enter a number to use these functions.")

	clearEntry()

def openMenu():
	root.destroy()
	import mgrmenu


## Display screen
numberEntry = Entry(root, font="none 30 bold", bg="light green", fg="black", justify=CENTER, width=12, show="*")
numberEntry.grid(column=1, row=2, columnspan=8, rowspan=2)

##numeric key buttons
numberButton = [ Button(root, image=photo[0], command=lambda: typeNumber(0), bg="silver", font="none 30 italic"),
Button(root, image=photo[1], command=lambda: typeNumber(1), bg="silver", font="none 30 italic"),
Button(root, image=photo[2], command=lambda: typeNumber(2), bg="silver", font="none 30 italic"),
Button(root, image=photo[3], command=lambda: typeNumber(3), bg="silver", font="none 30 italic"),
Button(root, image=photo[4], command=lambda: typeNumber(4), bg="silver", font="none 30 italic"),
Button(root, image=photo[5], command=lambda: typeNumber(5), bg="silver", font="none 30 italic"),
Button(root, image=photo[6], command=lambda: typeNumber(6), bg="silver", font="none 30 italic"),
Button(root, image=photo[7], command=lambda: typeNumber(7), bg="silver", font="none 30 italic"),
Button(root, image=photo[8], command=lambda: typeNumber(8), bg="silver", font="none 30 italic"),
Button(root, image=photo[9], command=lambda: typeNumber(9), bg="silver", font="none 30 italic"),
]

##basic operations buttons
clockInButton = Button(root, image=photo[11], command=clockIn, bg="light green", font="none 20 bold")
clockOutButton = Button(root, image=photo[12], command=clockOut, bg="orange", font="none 20 bold")
managerButton = Button(root, image=photo[13], command=openMenu, bg="light blue", font="none 20 italic")
## Reset command for managerButton once manager menu is to be programmed
clearButton = Button(root, image=photo[10], command=clearEntry, bg="silver", font="none 18 bold")




##Button Placements
numberButton[1].grid(column=3, row=6)
numberButton[2].grid(column=4, row=6)
numberButton[3].grid(column=5, row=6)
numberButton[4].grid(column=3, row=7)
numberButton[5].grid(column=4, row=7)
numberButton[6].grid(column=5, row=7)
numberButton[7].grid(column=3, row=8)
clearButton.grid(column=3, row=9)
numberButton[8].grid(column=4, row=8)
numberButton[9].grid(column=5, row=8)

numberButton[0].grid(column=4, row=9)
clockInButton.grid(column=5, row=9)
clockOutButton.grid(column=5, row=10)


## Program can only exit from the Manager Button
## for DEV purposes, managerButton is our Kill Switch
managerButton.grid(column=4, row=10)



##Onscreen Clock and Calendar
time1 = " "
def tick():
	global time1
	time2 = time.strftime('%H:%M:%S')
	if time2 != time1:
		time1 = time2
		clockDisplay.config(text=time2)
	clockDisplay.after(1000, tick)

theDate = " "
def cal():
	global theDate
	date2 = datetime.datetime.now()
	if date2 != theDate:
		theDate = date2
		dateDisplay.config(text=date2.strftime("%b %d, %Y (%a)"))
	dateDisplay.after(1000, cal)


clockDisplay = Label(root, font="Helvetica 32 bold", bg="black", fg="light green", justify=CENTER, width=11)
clockDisplay.grid(column=1, row=12, columnspan=6, rowspan=1)

dateDisplay = Label(root, font="Helvetica 22 bold", bg="black", fg="light green", justify=CENTER, width=15)
dateDisplay.grid(column=1, row=13, columnspan=6, rowspan=1)



tick()
cal()
root.mainloop()