from tkinter import *
from tkinter import ttk
import sys
import sqlite3

laborView = Tk()
laborView.title("Current Labor")
laborView.geometry("640x480")
laborView.resizable(width=False, height=False)

conn = sqlite3.connect("TestDB.db")
c = conn.cursor()
c2 = conn.cursor()

_exitButton = PhotoImage(file="formtools/exitButton.gif")
bgColor = "#34bfd4"

spread = ttk.Treeview(laborView)

laborView.config(bg=bgColor)

def killMe(event):
	laborView.destroy()

def killMeNow():
	laborView.destroy()

laborView.bind("<Escape>", killMe)

titleLabel = Label(laborView, text="Current Labor View", font="Helvetica 28 bold", fg="black", bg=bgColor, justify=CENTER).place(x=310,y=70,anchor=CENTER)

spread["columns"] = (0,1,2,3)
spread.column(0, width=80)
spread.column(1, width=50)
spread.column(2, width=50)
spread.column(3, width=50)


spread.heading(0, text = "ClockDate")
spread.heading(1, text = "EmpID")
spread.heading(2, text = "In Time")
spread.heading(3, text = "Pay Rate")

spread.place(x=312, y=199, anchor=CENTER)


exitButton = Button(laborView, image=_exitButton, command=killMeNow).place(x=320, y=415, anchor=CENTER)

count = c.execute("SELECT COUNT(*) FROM TimeClock WHERE isClockedIn = 1").fetchall()[0][0]
data = c.execute("SELECT * FROM TimeClock WHERE isClockedIn = 1").fetchall()

lastname = []
firstname = []

for i in range(0, count):
	##Read the names from the EmpTable where the ID numbers in TimeClock are ClockedIn
	##Take those names and return them to the propper array
	##output them in the aforementioned forloop.
	lastname.append(c.execute("SELECT EmpLst FROM EmpTable WHERE EmpID = " +str(data[i][1])).fetchall()[0][0])
	firstname.append(c.execute("SELECT EmpFst FROM EmpTable WHERE EmpID = " +str(data[i][1])).fetchall()[0][0])
	print(lastname)
	print(firstname)

clockIn = []
ID = []
inTime = []
payOut = []

for i in range(0, count):
	clockIn.append(data[i][0])
	ID.append(data[i][1])
	inTime.append(data[i][2])
	payOut.append(data[i][4])
	spread.insert("", i, text=lastname[i]+" "+firstname[i], values=(clockIn[i],ID[i], inTime[i], payOut[i]))

laborView.mainloop()