from tkinter import *
import sys

aboutWindow = Tk() ##change to Toplevel() upon integration
aboutWindow.title("About: TimeLord")
aboutWindow.resizable(height=False, width=False)
aboutWindow.geometry("640x480")


def killMe():
	aboutWindow.destroy()

aboutLogo = PhotoImage(file="aboutLogo.gif")
aboutTrim = PhotoImage(file="aboutTrim.gif")

trimLabel = Label(aboutWindow, image=aboutTrim) .place(x=0, y=0)
titleLabel = Label(aboutWindow, text="TimeLord®︎", font="Helvetica 48 bold") .place(x=25, y=15, anchor=NW)
byLineLabel = Label(aboutWindow, text="by Mitakasoft", font="Helvetica 12", justify=RIGHT) .place(x=175, y=65, anchor=NW)
versionLabel = Label(aboutWindow, text="Version Number: 0.ALPHA", font="Helvetica 18", justify=RIGHT) .place(x=315, y=60)

aboutIcon = Label(aboutWindow, image=aboutLogo) .place(x=14, y=120, anchor=NW)

legal1 = Label(aboutWindow, text="©︎2019 Mitakasoft\nNishitokyo, Tokyo, Japan\nhttps://mitakasoft.jp", font="helvetica 18", justify=LEFT) .place(x=29, y=355, anchor=NW)

licenseIntro = Label(aboutWindow, text="This software is\nlicensed to:", font="Helvetica 18", justify=LEFT) .place(x=227, y=126, anchor=NW)
licenseeName = Label(aboutWindow, text="Licensee Name", font="Helvetica 18", justify=RIGHT)  ##Editable so must be instantiated independently
licenseeName.place(x=533, y=146, anchor=NE)

licenseeAddyIntro = Label(aboutWindow, text="Licensee Address:", font="Helvetica 18", justify=LEFT) .place(x=227, y=183, anchor=NW)
licenseeAddy = Label(aboutWindow, text="1-2-3 Dokodemo\nMinato-Ku, Tokyo\n000-0000 Japan", font="Helvetica 18", justify=RIGHT)
licenseeAddy.place(x=533, y=184, anchor=NE)

legal2 = Label(aboutWindow, text="For exclusive use by the licensee\nonly.  Unauthorized use is strictly\nprohibited by law.", font="Helvetica 18", justify=RIGHT) .place(x=533, y=303, anchor=E)

credit = Label(aboutWindow, text="Built and created by\nMichael Yamazaki\nMade in Japan", font="Helvetica 18", justify=RIGHT) .place(x=533, y=389, anchor=E)

deathButton = Button(aboutWindow, text="   Close   ", command=killMe)
deathButton.place(x=269, y=439, anchor=NW)

aboutWindow.mainloop()