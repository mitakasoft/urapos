import datetime
import sqlite3
import sys
import time
from tkinter import *
from tkinter import messagebox, ttk
from xlwt import Formula, Workbook, easyxf

conn = sqlite3.connect("TestDB.db")
c = conn.cursor()
c2 = conn.cursor()

macFont = "Helvetica 14"
winFont = "Helveitca 12"
textColor = "white"


BG="#758efb"

paywin = Tk() ## Change to Toplevel() upon integration
paywin.title("View Payroll")
paywin.geometry("640x480")
paywin.resizable(width=False, height=False)
paywin.config(bg=BG)

searchImage = PhotoImage(file="payrollSearchButton.gif")
saveImage = PhotoImage(file="payrollSaveButton.gif")
exitImage = PhotoImage(file="payrollExitButton.gif")
clearResultsImage = PhotoImage(file="payrollClearResultsButton.gif")
clearImage = PhotoImage(file="payrollClearButton.gif")
dividerImage = PhotoImage(file="payrollDivider.gif")
backImage = PhotoImage(file="payrollStatusBack.gif")
ignoreImage = PhotoImage(file="payrollIgnoreButton.gif")
includeImage = PhotoImage(file="payrollIncludeButton.gif")

globalData = []
globalNames = []


##search bool
isSearchIgnore = False


def killMe(event):
	paywin.destroy()

def closePaywin():
	paywin.destroy()

def getHelp():
	backColor = "#989898"
	helpWin = Toplevel()
	helpWin.title("Help")
	helpWin.geometry("320x240")
	helpWin.resizable(width=False, height=False)
	helpWin.config(bg=backColor)

	helpGraphic = PhotoImage(file="helpGraphic.gif")

	def closeMe():
		helpWin.destroy()

	def suicideHotKey(event):
		closeMe()

	helpWin.bind("<Escape>", suicideHotKey)

	helpHeader = Label(helpWin, text="How to Search", bg=backColor, font=macFont) .place(x=160, y=16, anchor=CENTER)
	graphicLabel = Label(helpWin, image=helpGraphic, bg=backColor) .place(x=121, y=121, anchor=CENTER)
	helpExitButton = Button(helpWin, text="Exit (Esc)", command=closeMe, bg=backColor, width=10) .place(x=265, y=224, anchor=CENTER)
	helpStep1 = Label(helpWin, text="1. Tap this to set your\nsearch parameters", bg=backColor, font=macFont, justify=LEFT) .place(x=219, y=46, anchor=CENTER)
	helpStep2 = Label(helpWin, text="2. Enter an Employee ID\nor the employee's last\nname", bg=backColor, font=macFont, justify=LEFT) .place(x=228, y=95, anchor=CENTER)
	helpStep3 = Label(helpWin, text="3. Enter your pay period\nYYYMMDD and click\nthe Search button", bg=backColor, font=macFont, justify=LEFT) .place(x=227, y=151, anchor=CENTER)

	helpWin.mainloop()

def helpHotKey(event):
	getHelp()

paywin.bind("<F1>", helpHotKey)

def clearEntries():
	openEntry.delete(0,END)
	lastEntry.delete(0,END)
	closeEntry.delete(0,END)
	empIDEntry.delete(0,END)

def clearTable():
	global globalNames, globalData
	if messagebox.askyesno("Warning", "Clearing the table will clear results from memory.\n"+
		"You won't be able to use these results.\n"+
		"Clearing will also clear the entry fields below.\n\n"+
		"Do you wish to proceed?"):
		payTree.delete(*payTree.get_children())
		hoursLabel.config(text="0 hrs")
		payLabel.config(text="¥ 0")
		clearEntries()
		globalNames = []
		globalData = []


def ignoreSearch():
	global isSearchIgnore
	if isSearchIgnore:
		isSearchIgnore = False
		empIDEntry.config(state=NORMAL)
		lastEntry.config(state=NORMAL)
		ignoreButton.config(image=includeImage)
		result = "Search will INCLUDE ID and/or Name"
	else:
		isSearchIgnore = True
		empIDEntry.config(state=DISABLED)
		lastEntry.config(state=DISABLED)
		ignoreButton.config(image=ignoreImage)
		result = "Search will IGNORE ID and Name"


##Pay Tree
payTree = ttk.Treeview(paywin, height=13)
payTree.place(x=206, y=149, anchor=CENTER)

payTree["columns"] =(1, 2, 3, 4, 5, 6)
payTree.column("#0", width=0)
payTree.column(1, width=80)
payTree.column(2, width=80)
payTree.column(3, width=25)
payTree.column(4, width=25)
payTree.column(5, width=45)
payTree.column(6, width=100)

payTree.heading(1, text="Name")
payTree.heading(2, text="Date")
payTree.heading(3, text="Hrs")
payTree.heading(4, text="OT")
payTree.heading(5, text="Rate")
payTree.heading(6, text="Salary")

def fillTree():
	global globalNames, globalData
	payTree.delete(*payTree.get_children())
	ID = []
	names = []
	totalHours = []
	totalWages = []
	if isSearchIgnore:
		try:
			start = int(openEntry.get())
			end = int(closeEntry.get())
		
			count = c.execute("SELECT COUNT(*) FROM Payroll WHERE ClockDate BETWEEN "+str(start) +" AND "+str(end)).fetchone()[0]

			for i in range(count):
				data = c.execute("SELECT * FROM Payroll WHERE ClockDate BETWEEN " +str(start)+" AND " + str(end)).fetchall()
				ID.append(data[i][0])
				names.append(c.execute("SELECT EmpLst FROM EmpTable WHERE EmpID = " +str(ID[i])).fetchall()[0][0])
				payTree.insert("", i, values=(names[i], data[i][1], data[i][2], data[i][3], data[i][4], data[i][5]))
				totalHours.append(data[i][2])
				totalWages.append(data[i][5])
				globalNames.append(names)
				globalData.append(data)


			hoursLabel.config(text="{:,}".format(sum(totalHours))+" hours")
			payLabel.config(text="¥{:,}".format(sum(totalWages)))
			

		except ValueError:
			messagebox.showerror("No Information Entered", "Please enter a range.  If same day, enter the same date for both Payroll Open and Payroll Close dates.")

	else: 
		try:
			start = int(openEntry.get())
			end = int(closeEntry.get())
			if len(empIDEntry.get()) == 0:
				name = str(lastEntry.get())
				verify = c.execute('SELECT COUNT(*) FROM EmpTable WHERE EmpLst = "' + str(name)+'"').fetchall()[0][0]
				if int(verify) > 1:
					messagebox.showwarning("More Than One Found", "More than one entry found for name: " +name+".\nPlease use the Search function from the main menu to find the desired employee's ID number.")
				else:
					try:
						idGet = c.execute('SELECT EmpID FROM EmpTable WHERE EmpLst = "' + str(name)+'"').fetchone()[0]
						count = c.execute("SELECT COUNT(*) FROM Payroll WHERE EmpID = " +str(idGet) + " AND ClockDate BETWEEN "+str(start) + " AND " + str(end)).fetchone()[0]

						if count == 0:
							messagebox.showerror("Information Not Found", "Entries not found.  Please check your spelling or date numbers.")
						else:
							for i in range(count):
								data = c.execute("SELECT * FROM Payroll WHERE EmpID = " +str(idGet) + " AND ClockDate BETWEEN " + str(start) + " AND " + str(end)).fetchall()
								payTree.insert("", i, values=(name, data[i][1], data[i][2], data[i][3], data[i][4], data[i][5]))
								totalHours.append(data[i][2])
								totalWages.append(data[i][5])
								globalNames.append(name)
								globalData.append(data)

							hoursLabel.config(text="{:,}".format(sum(totalHours))+" hours")
							payLabel.config(text="¥{:,}".format(sum(totalWages)))



					except TypeError:
						messagebox.showerror("Not Found", "Your request was not found.  Please check the name and try again.  If you are unsure of the name, " + 
							"use the search function on the main menu to look up the employee number.")
			else:
				idSearch = int(empIDEntry.get())
				verify= c.execute("SELECT COUNT(*) FROM Payroll WHERE EmpID = " +str(idSearch)).fetchone()[0]
			
				if verify == 0: 
					messagebox.showerror("Not Found", "Employee number not found in payroll.")
				else:
					name = (c.execute("SELECT EmpLst FROM EmpTable WHERE EmpID = " +str(idSearch)).fetchone()[0] + " " + c.execute("SELECT EmpFst FROM EmpTable WHERE EmpID =" +str(idSearch)).fetchone()[0])
					count = c.execute("SELECT COUNT(*) FROM Payroll WHERE EmpID = " +str(idSearch) + " AND ClockDate BETWEEN "+str(start) + " AND " + str(end)).fetchone()[0]
					
					if count == 0:
						messagebox.showerror("Information Not Found", "Entries not found.  Please check your spelling or date numbers.")
					else:
						for i in range(count):
							data = c.execute("SELECT * FROM Payroll WHERE EmpID = " +str(idSearch)+" AND ClockDate BETWEEN " +str(start)+ " AND " + str(end)).fetchall()
							payTree.insert("", i, values=(name, data[i][1], data[i][2], data[i][3], data[i][4], data[i][5]))
							totalHours.append(data[i][2])
							totalWages.append(data[i][5])
							globalNames.append(name)
							globalData.append(data)

						hoursLabel.config(text="{:,}".format(sum(totalHours))+" hours")
						payLabel.config(text="¥{:,}".format(sum(totalWages)))

		except ValueError:
			messagebox.showerror("More Data Needed", "You have missing information.  Please check to make sure you have:\nName\nor ID Number\nOpen and Closing Payroll Dates.")




##graphicals
statusBack = Label(paywin, image=backImage) .place(x=518, y=149, anchor=CENTER)
divider = Label(paywin, image=dividerImage) .place(x=319, y=380, anchor=CENTER)

##buttons
exitButton = Button(paywin, image=exitImage, command=closePaywin) .place(x=557, y=434, anchor=CENTER)
clearButton = Button(paywin, image=clearImage, command=clearEntries) .place(x=422, y=425, anchor=CENTER)
searchButton = Button(paywin, image=searchImage, command=fillTree) .place(x=283, y=425, anchor=CENTER)
clearResultsButton = Button(paywin, image=clearResultsImage, command=clearTable) .place(x=516, y=180, anchor=CENTER)
ignoreButton = Button(paywin, image=includeImage, command=ignoreSearch)
ignoreButton.place(x=585, y=340, anchor=CENTER)
helpButton = Button(paywin, text="Help (F1)", width=11, command=getHelp) .place(x=557, y=396, anchor=CENTER)

##Constant Headers
sectionHeader = Label(paywin, text="SEARCH PAYROLL", fg=textColor, font=macFont, bg=BG) .place(x=76, y=300, anchor=CENTER)
hoursHeader = Label(paywin, text="Total Hours:", fg=textColor, font=macFont, bg=BG) .place(x=479, y=53, anchor=CENTER)
payoutHeader = Label(paywin, text="Total Payout:", fg=textColor, font=macFont, bg=BG) .place(x=483, y=107, anchor=CENTER)
empHeader = Label(paywin, text="Employee ID", fg=textColor, font=macFont, bg=BG) .place(x=99, y=320, anchor=CENTER)
openHeader = Label(paywin, text="Payroll Open Date", fg=textColor, font=macFont, bg=BG) .place(x=283, y=320, anchor=CENTER)
toHeader = Label(paywin, text="to", fg=textColor, font=macFont, bg=BG) .place(x=380, y=343, anchor=CENTER)
closeHeader = Label(paywin, text="Payroll Close Date", fg=textColor, font=macFont, bg=BG) .place(x=480, y=320, anchor=CENTER)
orHeader = Label(paywin, text="or", fg=textColor, font=macFont, bg=BG) .place(x=96, y=380, anchor=CENTER)
lastHeader = Label(paywin, text="Last Name", fg=textColor, font=macFont, bg=BG) .place(x=99, y=409, anchor=CENTER)


##Clock and Calendar Movements
clockTime = None
calendarDate = None

def tick():
	global clockTime
	clockTime = " "
	time2 = time.strftime("%H:%M:%S")
	if time2 != clockTime:
		clockTime = time2
		clockDisplay.config(text=time2)
	clockDisplay.after(1000,tick)

def track():
	global calendarDate
	calendarDate = " "
	date2 = datetime.datetime.now()
	if date2 != calendarDate:
		calendarDate = date2
		calendarDisplay.config(text=date2.strftime("%b %d, %Y (%a)"))
	calendarDisplay.after(1000, track)



##Clock and Calendar Labels
clockDisplay = Label(paywin, font=macFont, bg=BG, fg=textColor, justify=CENTER)
clockDisplay.place(x=512, y=248, anchor=CENTER)
calendarDisplay = Label(paywin, font=macFont, bg=BG, fg=textColor, justify=CENTER)
calendarDisplay.place(x=518, y=265, anchor=CENTER)

##moving the clock and calendar
tick()
track()

##Manipulatable Labels
hoursLabel = Label(paywin, text="0 hrs", font=macFont, fg=textColor, bg=BG)
payLabel = Label(paywin, text="¥ 0", font=macFont, fg=textColor, bg=BG)
hoursLabel.place(x=573, y=53, anchor=CENTER)
payLabel.place(x=573, y=107, anchor=CENTER)

##Entries
empIDEntry = Entry(paywin, font=macFont, width=12, justify=CENTER)
lastEntry = Entry(paywin, font=macFont, width=12, justify=CENTER)
openEntry = Entry(paywin, font=macFont, width=12, justify=CENTER)
closeEntry = Entry(paywin, font=macFont, width=12, justify=CENTER)

empIDEntry.place(x=99, y=343, anchor=CENTER)
lastEntry.place(x=99, y=429, anchor=CENTER)
openEntry.place(x=283, y=343, anchor=CENTER)
closeEntry.place(x=480, y=343, anchor=CENTER)

paywin.bind("<Escape>", killMe)










paywin.mainloop()
