import sqlite3
import sys
import random
from tkinter import *
from tkinter import ttk

thisShit = Tk()
tree = ttk.Treeview(thisShit)
tree.pack()


tree["columns"] =(1, 2, 3, 4, 5, 6)
tree.column(1, width=100)
tree.column(2, width=100)
tree.column(3, width=100)
tree.column(4, width=100)
tree.column(5, width=100)
tree.column(6, width=100)
tree.heading(1, text="EmpID")
tree.heading(2, text="ClockDate")
tree.heading(3, text="HoursWorked")
tree.heading(4, text="OTHours")
tree.heading(5, text="EmpPay")
tree.heading(6, text="EntitledPay")

def killMe(event):
	thisShit.destroy()

thisShit.bind("<Escape>", killMe)


conn = sqlite3.connect('PayRollTestDB.db')
c = conn.cursor()

c.execute("CREATE TABLE IF NOT EXISTS Payroll(EmpID REAL, ClockDate REAL, HoursWorked REAL, OTHours REAL, EmpPay REAL, EntitledPay REAL)")

##Numbers inputted will be simulated for testing

EmpID = 0
ClockDate = 0
HoursWorked = 0
EmpPay = 0
OverTimeHours = 0
EntitledPay = 0

def getValues(ID, Date):
	global ClockDate, HoursWorked, EmpPay, OverTimeHours, EntitledPay
	OverTimeRate = 0
	OTPay = 0
	TotalPay = 0
	EmpID = ID
	ClockDate = Date 
	EmpPay = random.randrange(900, 3500)
	HoursWorked = random.randrange(5, 13)
	if HoursWorked >= 8:
		OverTimeHours = HoursWorked - 8
		OverTimeRate = EmpPay * .5
		OTPay = OverTimeRate * OverTimeHours
	
	EntitledPay = int((HoursWorked * EmpPay) + OTPay)

	print(EmpID)
	print(ClockDate)
	print(HoursWorked)
	print(EmpPay)
	print(OverTimeHours)
	print(EntitledPay)

	c.execute("INSERT INTO Payroll (EmpID, ClockDate, HoursWorked, OTHours, EmpPay, EntitledPay) VALUES (?,?,?,?,?,?)", (EmpID, ClockDate, HoursWorked, OverTimeHours, EmpPay, EntitledPay))
	conn.commit()


##Get and test general output no search function
def readValues(): 
	ID = []
	Date = []
	Hours = []
	OT = []
	Pay = []
	PayOut = []

	data = c.execute("SELECT * FROM Payroll").fetchall()
	count = c.execute("SELECT COUNT (*) FROM Payroll").fetchall()[0][0]

	for i in range(0, count):
		ID.append(data[i][0])
		Date.append(data[i][1])
		Hours.append(data[i][2])
		OT.append(data[i][3])
		Pay.append(data[i][4])
		PayOut.append(data[i][5])
		tree.insert("", i, values=(ID[i], Date[i], Hours[i], OT[i], Pay[i], PayOut[i]))




##Runs a search with given parameters and delivers the information into the table for viewing
def runSearch(ID, openDate, closeDate):
	searchq = c.execute("SELECT * FROM Payroll WHERE EmpID = "+str(ID)+" AND ClockDate BETWEEN " + str(openDate)+" AND " +str(closeDate)).fetchall()
	count = c.execute("SELECT COUNT(*) FROM Payroll WHERE EmpID = "+str(ID)+" AND ClockDate BETWEEN " + str(openDate)+" AND " +str(closeDate)).fetchall()[0][0]
	print(count)
	print(searchq)

	##print search to tree
	Date = []
	Hours = []
	OT = []
	Pay = []
	PayOut = []

	for i in range(0, count):
		Date.append(searchq[i][1])
		Hours.append(searchq[i][2])
		OT.append(searchq[i][3])
		Pay.append(searchq[i][4])
		PayOut.append(searchq[i][5])
		tree.insert("",i, text="LastName FirstName", values=(ID, Date[i], Hours[i], OT[i], Pay[i], PayOut[i]))

	print("Total Pay: ¥"+str(sum(PayOut)))



runSearch(1701, 20181125, 20181130)

thisShit.mainloop()