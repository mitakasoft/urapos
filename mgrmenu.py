from tkinter import *
from tkinter import messagebox
from tkinter import ttk
import datetime
import time
import sys
import os
import subprocess
import sqlite3
import random

## The main manager menu - the initial program that will launch when the
## application is initially loaded

##IMPORTANT: Readjusted measurements in the code, re-coded in WINDOWS
##HUGE Measurement discrepencies between Mac and Windows versions
##One version does NOT fit all, and will have to make minor adjustments
##between the platforms.

##THIS one is macOS Optimized

##Common coordinate variables
col1X = 144
col2X = 285
col3X = 428

empY = 74
dbY = 174
admY = 274

macFont = "Helvetica 14"
winFont = "Helveitca 12"
textColor = "white"

##search bool
isSearchIgnore = False

clockedIn = 0
laborCost = 0
laborLabel = "¥" + str(laborCost)

customerName = "Licensee Name"

bgColor = '#253340'
borderGray = '#757272'
mainFont = "Helvetica 18"

macSize = "572x415"
winSize = " "

##Set up for real-time receiving of labor data
conn = sqlite3.connect('TestDB.db')
c = conn.cursor()
c2 = conn.cursor()

c.execute("CREATE TABLE IF NOT EXISTS TimeClock(ClockDate NUMERIC, EmpID INTEGER, ClockIN NUMERIC, ClockOUT NUMERIC, EmpPay NUMERIC, isClockedIn NUMERIC)")
c.execute("CREATE TABLE IF NOT EXISTS Payroll(EmpID INTEGER, ClockDate NUMERIC, HoursWorked NUMERIC, OTHours NUMERIC, EmpPay NUMERIC, EntitledPay NUMERIC)")


root = Tk()
root.geometry(macSize)
root.title("TimeLord")
root.resizable(width=False, height=False)


## UI Images
UIAbout = PhotoImage(file="ui/UIAbout.gif")
UIadmin = PhotoImage(file="ui/UIadmin.gif")
UIbg = PhotoImage(file="ui/UIbg.gif")
UIbottomBanner = PhotoImage(file="ui/UIbottomBanner.gif")
UIdbase = PhotoImage(file="ui/UIdbase.gif")
UIedit = PhotoImage(file="ui/UIedit.gif")
UIemployee = PhotoImage(file="ui/UIemployee.gif")
UIexit = PhotoImage(file="ui/UIexit.gif")
UIlabor = PhotoImage(file="ui/UIlabor.gif")
UIpayroll = PhotoImage(file="ui/UIpayroll.gif")
UIadd = PhotoImage(file="ui/HR.gif")
UIsearch = PhotoImage(file="ui/UIsearch.gif")
UIsettings = PhotoImage(file="ui/UIsettings.gif")
UIstatus = PhotoImage(file="ui/UIstatus.gif")
UItimeclock = PhotoImage(file="ui/UItimeclock.gif")
UItopBanner = PhotoImage(file="ui/UItopBanner.gif")



def killMe():
	if messagebox.askyesno("Confirm", "Are you sure you wish to quit?"):
		exit()

def getPayroll():
	BG="#758efb"

	paywin = Toplevel()
	paywin.title("View Payroll")
	paywin.geometry("640x480")
	paywin.resizable(width=False, height=False)
	paywin.config(bg=BG)

	searchImage = PhotoImage(file="payrollSearchButton.gif")
	saveImage = PhotoImage(file="payrollSaveButton.gif")
	exitImage = PhotoImage(file="payrollExitButton.gif")
	clearResultsImage = PhotoImage(file="payrollClearResultsButton.gif")
	clearImage = PhotoImage(file="payrollClearButton.gif")
	dividerImage = PhotoImage(file="payrollDivider.gif")
	backImage = PhotoImage(file="payrollStatusBack.gif")
	ignoreImage = PhotoImage(file="payrollIgnoreButton.gif")
	includeImage = PhotoImage(file="payrollIncludeButton.gif")

	globalData = []
	globalNames = []


	


	def killMe(event):
		paywin.destroy()

	def closePaywin():
		paywin.destroy()

	def getHelp():
		backColor = "#989898"
		helpWin = Toplevel()
		helpWin.title("Help")
		helpWin.geometry("320x240")
		helpWin.resizable(width=False, height=False)
		helpWin.config(bg=backColor)

		helpGraphic = PhotoImage(file="helpGraphic.gif")

		def closeMe():
			helpWin.destroy()

		def suicideHotKey(event):
			closeMe()

		helpWin.bind("<Escape>", suicideHotKey)

		helpHeader = Label(helpWin, text="How to Search", bg=backColor, font=macFont) .place(x=160, y=16, anchor=CENTER)
		graphicLabel = Label(helpWin, image=helpGraphic, bg=backColor) .place(x=121, y=121, anchor=CENTER)
		helpExitButton = Button(helpWin, text="Exit (Esc)", command=closeMe, bg=backColor, width=10) .place(x=265, y=224, anchor=CENTER)
		helpStep1 = Label(helpWin, text="1. Tap this to set your\nsearch parameters", bg=backColor, font=macFont, justify=LEFT) .place(x=219, y=46, anchor=CENTER)
		helpStep2 = Label(helpWin, text="2. Enter an Employee ID\nor the employee's last\nname", bg=backColor, font=macFont, justify=LEFT) .place(x=228, y=95, anchor=CENTER)
		helpStep3 = Label(helpWin, text="3. Enter your pay period\nYYYMMDD and click\nthe Search button", bg=backColor, font=macFont, justify=LEFT) .place(x=227, y=151, anchor=CENTER)

		helpWin.mainloop()

	def helpHotKey(event):
		getHelp()

	paywin.bind("<F1>", helpHotKey)

	def clearEntries():
		openEntry.delete(0,END)
		lastEntry.delete(0,END)
		closeEntry.delete(0,END)
		empIDEntry.delete(0,END)

	def clearTable():
		global globalNames, globalData
		if messagebox.askyesno("Warning", "Clearing the table will clear results from memory.\n"+
			"You won't be able to use these results.\n"+
			"Clearing will also clear the entry fields below.\n\n"+
			"Do you wish to proceed?"):
			payTree.delete(*payTree.get_children())
			hoursLabel.config(text="0 hrs")
			payLabel.config(text="¥ 0")
			clearEntries()
			globalNames = []
			globalData = []


	def ignoreSearch():
		global isSearchIgnore
		if isSearchIgnore:
			isSearchIgnore = False
			empIDEntry.config(state=NORMAL)
			lastEntry.config(state=NORMAL)
			ignoreButton.config(image=includeImage)
			result = "Search will INCLUDE ID and/or Name"
		else:
			isSearchIgnore = True
			empIDEntry.config(state=DISABLED)
			lastEntry.config(state=DISABLED)
			ignoreButton.config(image=ignoreImage)
			result = "Search will IGNORE ID and Name"


	##Pay Tree
	payTree = ttk.Treeview(paywin, height=13)
	payTree.place(x=206, y=149, anchor=CENTER)

	payTree["columns"] =(1, 2, 3, 4, 5, 6)
	payTree.column("#0", width=0)
	payTree.column(1, width=80)
	payTree.column(2, width=80)
	payTree.column(3, width=25)
	payTree.column(4, width=25)
	payTree.column(5, width=45)
	payTree.column(6, width=100)

	payTree.heading(1, text="Name")
	payTree.heading(2, text="Date")
	payTree.heading(3, text="Hrs")
	payTree.heading(4, text="OT")
	payTree.heading(5, text="Rate")
	payTree.heading(6, text="Salary")

	def fillTree():
		global globalNames, globalData
		payTree.delete(*payTree.get_children())
		ID = []
		names = []
		totalHours = []
		totalWages = []
		if isSearchIgnore:
			try:
				start = int(openEntry.get())
				end = int(closeEntry.get())
			
				count = c.execute("SELECT COUNT(*) FROM Payroll WHERE ClockDate BETWEEN "+str(start) +" AND "+str(end)).fetchone()[0]

				for i in range(count):
					data = c.execute("SELECT * FROM Payroll WHERE ClockDate BETWEEN " +str(start)+" AND " + str(end)).fetchall()
					ID.append(data[i][0])
					names.append(c.execute("SELECT EmpLst FROM EmpTable WHERE EmpID = " +str(ID[i])).fetchall()[0][0])
					payTree.insert("", i, values=(names[i], data[i][1], data[i][2], data[i][3], data[i][4], data[i][5]))
					totalHours.append(data[i][2])
					totalWages.append(data[i][5])
					


				hoursLabel.config(text="{:,}".format(sum(totalHours))+" hours")
				payLabel.config(text="¥{:,}".format(sum(totalWages)))
				

			except ValueError:
				messagebox.showerror("No Information Entered", "Please enter a range.  If same day, enter the same date for both Payroll Open and Payroll Close dates.")

		else: 
			try:
				start = int(openEntry.get())
				end = int(closeEntry.get())
				if len(empIDEntry.get()) == 0:
					name = str(lastEntry.get())
					verify = c.execute('SELECT COUNT(*) FROM EmpTable WHERE EmpLst = "' + str(name)+'"').fetchall()[0][0]
					if int(verify) > 1:
						messagebox.showwarning("More Than One Found", "More than one entry found for name: " +name+".\nPlease use the Search function from the main menu to find the desired employee's ID number.")
					else:
						try:
							idGet = c.execute('SELECT EmpID FROM EmpTable WHERE EmpLst = "' + str(name)+'"').fetchone()[0]
							count = c.execute("SELECT COUNT(*) FROM Payroll WHERE EmpID = " +str(idGet) + " AND ClockDate BETWEEN "+str(start) + " AND " + str(end)).fetchone()[0]

							if count == 0:
								messagebox.showerror("Information Not Found", "Entries not found.  Please check your spelling or date numbers.")
							else:
								for i in range(count):
									data = c.execute("SELECT * FROM Payroll WHERE EmpID = " +str(idGet) + " AND ClockDate BETWEEN " + str(start) + " AND " + str(end)).fetchall()
									payTree.insert("", i, values=(name, data[i][1], data[i][2], data[i][3], data[i][4], data[i][5]))
									totalHours.append(data[i][2])
									totalWages.append(data[i][5])
									
								hoursLabel.config(text="{:,}".format(sum(totalHours))+" hours")
								payLabel.config(text="¥{:,}".format(sum(totalWages)))



						except TypeError:
							messagebox.showerror("Not Found", "Your request was not found.  Please check the name and try again.  If you are unsure of the name, " + 
								"use the search function on the main menu to look up the employee number.")
				else:
					idSearch = int(empIDEntry.get())
					verify= c.execute("SELECT COUNT(*) FROM Payroll WHERE EmpID = " +str(idSearch)).fetchone()[0]
				
					if verify == 0: 
						messagebox.showerror("Not Found", "Employee number not found in payroll.")
					else:
						name = (c.execute("SELECT EmpLst FROM EmpTable WHERE EmpID = " +str(idSearch)).fetchone()[0] + " " + c.execute("SELECT EmpFst FROM EmpTable WHERE EmpID =" +str(idSearch)).fetchone()[0])
						count = c.execute("SELECT COUNT(*) FROM Payroll WHERE EmpID = " +str(idSearch) + " AND ClockDate BETWEEN "+str(start) + " AND " + str(end)).fetchone()[0]
						
						if count == 0:
							messagebox.showerror("Information Not Found", "Entries not found.  Please check your spelling or date numbers.")
						else:
							for i in range(count):
								data = c.execute("SELECT * FROM Payroll WHERE EmpID = " +str(idSearch)+" AND ClockDate BETWEEN " +str(start)+ " AND " + str(end)).fetchall()
								payTree.insert("", i, values=(name, data[i][1], data[i][2], data[i][3], data[i][4], data[i][5]))
								totalHours.append(data[i][2])
								totalWages.append(data[i][5])
								
							hoursLabel.config(text="{:,}".format(sum(totalHours))+" hours")
							payLabel.config(text="¥{:,}".format(sum(totalWages)))

			except ValueError:
				messagebox.showerror("More Data Needed", "You have missing information.  Please check to make sure you have:\nName\nor ID Number\nOpen and Closing Payroll Dates.")




	##graphicals
	statusBack = Label(paywin, image=backImage) .place(x=518, y=149, anchor=CENTER)
	divider = Label(paywin, image=dividerImage) .place(x=319, y=380, anchor=CENTER)

	##buttons
	exitButton = Button(paywin, image=exitImage, command=closePaywin) .place(x=557, y=434, anchor=CENTER)
	clearButton = Button(paywin, image=clearImage, command=clearEntries) .place(x=422, y=425, anchor=CENTER)
	searchButton = Button(paywin, image=searchImage, command=fillTree) .place(x=283, y=425, anchor=CENTER)
	clearResultsButton = Button(paywin, image=clearResultsImage, command=clearTable) .place(x=516, y=180, anchor=CENTER)
	ignoreButton = Button(paywin, image=includeImage, command=ignoreSearch)
	ignoreButton.place(x=585, y=340, anchor=CENTER)
	helpButton = Button(paywin, text="Help (F1)", width=11, command=getHelp) .place(x=557, y=396, anchor=CENTER)

	##Constant Headers
	sectionHeader = Label(paywin, text="SEARCH PAYROLL", fg=textColor, font=macFont, bg=BG) .place(x=76, y=300, anchor=CENTER)
	hoursHeader = Label(paywin, text="Total Hours:", fg=textColor, font=macFont, bg=BG) .place(x=479, y=53, anchor=CENTER)
	payoutHeader = Label(paywin, text="Total Payout:", fg=textColor, font=macFont, bg=BG) .place(x=483, y=107, anchor=CENTER)
	empHeader = Label(paywin, text="Employee ID", fg=textColor, font=macFont, bg=BG) .place(x=99, y=320, anchor=CENTER)
	openHeader = Label(paywin, text="Payroll Open Date", fg=textColor, font=macFont, bg=BG) .place(x=283, y=320, anchor=CENTER)
	toHeader = Label(paywin, text="to", fg=textColor, font=macFont, bg=BG) .place(x=380, y=343, anchor=CENTER)
	closeHeader = Label(paywin, text="Payroll Close Date", fg=textColor, font=macFont, bg=BG) .place(x=480, y=320, anchor=CENTER)
	orHeader = Label(paywin, text="or", fg=textColor, font=macFont, bg=BG) .place(x=96, y=380, anchor=CENTER)
	lastHeader = Label(paywin, text="Last Name", fg=textColor, font=macFont, bg=BG) .place(x=99, y=409, anchor=CENTER)


	##Clock and Calendar Movements
	clockTime = None
	calendarDate = None

	def tick():
		global clockTime
		clockTime = " "
		time2 = time.strftime("%H:%M:%S")
		if time2 != clockTime:
			clockTime = time2
			clockDisplay.config(text=time2)
		clockDisplay.after(1000,tick)

	def track():
		global calendarDate
		calendarDate = " "
		date2 = datetime.datetime.now()
		if date2 != calendarDate:
			calendarDate = date2
			calendarDisplay.config(text=date2.strftime("%b %d, %Y (%a)"))
		calendarDisplay.after(1000, track)



	##Clock and Calendar Labels
	clockDisplay = Label(paywin, font=macFont, bg=BG, fg=textColor, justify=CENTER)
	clockDisplay.place(x=512, y=248, anchor=CENTER)
	calendarDisplay = Label(paywin, font=macFont, bg=BG, fg=textColor, justify=CENTER)
	calendarDisplay.place(x=518, y=265, anchor=CENTER)

	##moving the clock and calendar
	tick()
	track()

	##Manipulatable Labels
	hoursLabel = Label(paywin, text="0 hrs", font=macFont, fg=textColor, bg=BG)
	payLabel = Label(paywin, text="¥ 0", font=macFont, fg=textColor, bg=BG)
	hoursLabel.place(x=573, y=53, anchor=CENTER)
	payLabel.place(x=573, y=107, anchor=CENTER)

	##Entries
	empIDEntry = Entry(paywin, font=macFont, width=12, justify=CENTER)
	lastEntry = Entry(paywin, font=macFont, width=12, justify=CENTER)
	openEntry = Entry(paywin, font=macFont, width=12, justify=CENTER)
	closeEntry = Entry(paywin, font=macFont, width=12, justify=CENTER)

	empIDEntry.place(x=99, y=343, anchor=CENTER)
	lastEntry.place(x=99, y=429, anchor=CENTER)
	openEntry.place(x=283, y=343, anchor=CENTER)
	closeEntry.place(x=480, y=343, anchor=CENTER)

	paywin.bind("<Escape>", killMe)










	paywin.mainloop()


def getCurrentLabor():
	count = c.execute("SELECT COUNT(*) FROM TimeClock WHERE isClockedIn = 1").fetchall()[0][0]

	if count == 0:
		messagebox.showinfo("Empty", "No one is currently clocked-in.")

	else:
		laborView = Toplevel()
		laborView.title("Current Labor")
		laborView.geometry("640x480")
		laborView.resizable(width=False, height=False)

		_exitButton = PhotoImage(file="formtools/exitButton.gif")
		bgColor = "#34bfd4"

		spread = ttk.Treeview(laborView)

		laborView.config(bg=bgColor)

		def killMe(event):
			laborView.destroy()

		def killMeNow():
			laborView.destroy()

		laborView.bind("<Escape>", killMe)

		titleLabel = Label(laborView, text="Current Labor View", font="Helvetica 28 bold", fg="black", bg=bgColor, justify=CENTER).place(x=310,y=70,anchor=CENTER)

		spread["columns"] = (0,1,2,3)
		spread.column(0, width=80)
		spread.column(1, width=50)
		spread.column(2, width=50)
		spread.column(3, width=50)


		spread.heading(0, text = "ClockDate")
		spread.heading(1, text = "EmpID")
		spread.heading(2, text = "In Time")
		spread.heading(3, text = "Pay Rate")

		spread.place(x=312, y=199, anchor=CENTER)


		exitButton = Button(laborView, image=_exitButton, command=killMeNow).place(x=320, y=415, anchor=CENTER)

		
		data = c.execute("SELECT * FROM TimeClock WHERE isClockedIn = 1").fetchall()

		lastname = []
		firstname = []

		for i in range(0, count):
			##Read the names from the EmpTable where the ID numbers in TimeClock are ClockedIn
			##Take those names and return them to the propper array
			##output them in the aforementioned forloop.
			lastname.append(c.execute("SELECT EmpLst FROM EmpTable WHERE EmpID = " +str(data[i][1])).fetchall()[0][0])
			firstname.append(c.execute("SELECT EmpFst FROM EmpTable WHERE EmpID = " +str(data[i][1])).fetchall()[0][0])

		clockIn = []
		ID = []
		inTime = []
		payOut = []

		for i in range(0, count):
			clockIn.append(data[i][0])
			ID.append(data[i][1])
			inTime.append(data[i][2])
			payOut.append(data[i][4])
			spread.insert("", i, text=lastname[i]+" "+firstname[i], values=(clockIn[i],ID[i], inTime[i], payOut[i]))


		laborView.mainloop()




def getAbout():
	aboutWindow = Toplevel()
	aboutWindow.title("About: TimeLord")
	aboutWindow.resizable(height=False, width=False)
	aboutWindow.geometry("640x480")


	def killMe():
		aboutWindow.destroy()

	aboutLogo = PhotoImage(file="aboutLogo.gif")
	aboutTrim = PhotoImage(file="aboutTrim.gif")

	trimLabel = Label(aboutWindow, image=aboutTrim) .place(x=0, y=0)
	titleLabel = Label(aboutWindow, text="TimeLord®︎", font="Helvetica 48 bold") .place(x=25, y=15, anchor=NW)
	byLineLabel = Label(aboutWindow, text="by Mitakasoft", font="Helvetica 12", justify=RIGHT) .place(x=175, y=65, anchor=NW)
	versionLabel = Label(aboutWindow, text="Version Number: 0.ALPHA", font="Helvetica 18", justify=RIGHT) .place(x=315, y=60)

	aboutIcon = Label(aboutWindow, image=aboutLogo) .place(x=14, y=120, anchor=NW)

	legal1 = Label(aboutWindow, text="©︎2019 Mitakasoft\nNishitokyo, Tokyo, Japan\nhttps://mitakasoft.jp", font="helvetica 18", justify=LEFT) .place(x=29, y=355, anchor=NW)

	licenseIntro = Label(aboutWindow, text="This software is\nlicensed to:", font="Helvetica 18", justify=LEFT) .place(x=227, y=126, anchor=NW)
	licenseeName = Label(aboutWindow, text=customerName, font="Helvetica 18", justify=RIGHT)  ##Editable so must be instantiated independently
	licenseeName.place(x=533, y=146, anchor=NE)

	licenseeAddyIntro = Label(aboutWindow, text="Licensee Address:", font="Helvetica 18", justify=LEFT) .place(x=227, y=183, anchor=NW)
	licenseeAddy = Label(aboutWindow, text="1-2-3 Dokodemo\nMinato-Ku, Tokyo\n000-0000 Japan", font="Helvetica 18", justify=RIGHT)
	licenseeAddy.place(x=533, y=184, anchor=NE)

	legal2 = Label(aboutWindow, text="For exclusive use by the licensee\nonly.  Unauthorized use is strictly\nprohibited by law.", font="Helvetica 18", justify=RIGHT) .place(x=533, y=303, anchor=E)

	credit = Label(aboutWindow, text="Built and created by\nMichael Yamazaki\nMade in Japan", font="Helvetica 18", justify=RIGHT) .place(x=533, y=389, anchor=E)

	deathButton = Button(aboutWindow, text="   Close   ", command=killMe)
	deathButton.place(x=269, y=439, anchor=NW)

	aboutWindow.mainloop()



def addNew():
	windowType = "Create Employee"

	bgColor = '#253340'
	borderGray = '#757272'
	mainFont = "Helvetica 18"

	rootWindow = Toplevel()
	rootWindow.geometry("640x480")
	rootWindow.resizable(width=False, height=False)
	rootWindow.title(windowType)

	bg = PhotoImage(file="formtools/background.gif")
	bgImage = Label(rootWindow, image=bg).place(x=-3, y=-3)

	##REMOVE FOR INTEGRATION##
	
	def killMe():
		rootWindow.destroy()

	def destroyMe(event):
		killMe()

	rootWindow.bind("<Escape>", destroyMe)

	def resetEntries():
		if messagebox.askyesno("Confirm", "Are you sure you want to reset the form?"):
			numberEntry.delete(0,END)
			lastEntry.delete(0,END)
			firstEntry.delete(0,END)
			YOBEntry.delete(0,END)
			MOBEntry.delete(0,END)
			DOBEntry.delete(0,END)
			countryEntry.delete(0,END)
			phoneEntry.delete(0,END)
			visaEntry.delete(0,END)
			posEntry.delete(0,END)
			wageEntry.delete(0,END)
			statusEntry.delete(0,END)

	def hardReset():
		numberEntry.delete(0,END)
		lastEntry.delete(0,END)
		firstEntry.delete(0,END)
		YOBEntry.delete(0,END)
		MOBEntry.delete(0,END)
		DOBEntry.delete(0,END)
		countryEntry.delete(0,END)
		phoneEntry.delete(0,END)
		visaEntry.delete(0,END)
		posEntry.delete(0,END)
		wageEntry.delete(0,END)
		statusEntry.delete(0,END)


	def checkNumber():
		try:
			verify = numberEntry.get()
			if int(verify) < 999 or int(verify) > 9999:
				messagebox.showerror("Invalid Entry", "Numbers cannot be less than 1000 or greater than 9999, and must contain 4 digits.")
			else:
				checkNumber = c.execute("SELECT Count(*) FROM EmpTable WHERE EmpID = " +str(verify)).fetchone()[0]
				if checkNumber >= 1:
					messagebox.showerror("Conflict", str(verify)+" already exists in the database.  Please issue a different number or randomize.")
				else:
					messagebox.showwarning("It's Good", str(verify)+" is good to use.")
		except ValueError:
			messagebox.showerror("Nothing Entered", "You must enter a number in this field.")

	def getRandom():
		numberEntry.delete(0,END)
		randEntry = random.randrange(1000, 9999)
		verify = c.execute("SELECT Count(*) FROM EmpTable WHERE EmpID = " +str(randEntry)).fetchone()[0]
		while verify >= 1:
			randEntry = random.randrange(1000, 9999)
			reverify = c.execute("SELECT COUNT (*) FROM EmpTable WHERE EmpID = " + str(randEntry)).fetchone()[0]
		numberEntry.insert(0, str(randEntry))

	def registerPress():
		try:
			checkNumber()
			checkCountry()
		except ValueError:
			messagebox.showerror("Incomplete Input", "There are blank fields in the form.  Make sure all information is completely filled in before submission.")

	def completeRegistry():
		try:
			ID = numberEntry.get()
			lastName = lastEntry.get()
			firstName = firstEntry.get()
			YOB = int(YOBEntry.get())
			MOB = int(MOBEntry.get())
			if MOB < 1 or MOB > 12:
				messagebox.showerror("Calendar Error", "There are only 12 months in a year.  Please enter 1 through 12 to indicate the month.")
			else:
				DOB = int(DOBEntry.get())
				if DOB < 1 or DOB > 32:
					messagebox.showerror("Calendar Error", "Months with 30 days: April (4), June (6), September (9), November (11).\nIrregular Month: February (2)\nAll other months: 31 Days.\nNo month has "+str(DOB)+" days.")
				else:
					phoneNumber = phoneEntry.get()
					country = countryEntry.get()
					theVisa = visaEntry.get()
					position = posEntry.get()
					pay = wageEntry.get()
					empStatus = statusEntry.get()

					if messagebox.askyesno("Ready to Register", "Would you like to register " + lastName+", "+firstName+"?"):
						c.execute("INSERT INTO EmpTable(EmpID, EmpLst, EmpFst, EmpYOB, EmpMOB, EmpDOB, EmpPho, EmpCou, EmpVsa, EmpPos, EmpPay, EmpSts) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
							(ID, lastName, firstName, YOB, MOB, DOB, phoneNumber, country, theVisa, position, pay, empStatus))
						conn.commit()
						messagebox.showinfo("Entry Successful", lastName+", "+firstName+"'s registry was completed successfully.")
						hardReset()
					else:
						messagebox.showwarning("Aborted", "Submission cancelled.")
		except ValueError:
			messagebox.showerror("Error", "Some data was invalid or incomplete.\nPlease verify and resubmit the form.")



	def checkCountry():
		country = countryEntry.get()
		if country != "JAPAN" or country!= "Japan" or country != "japan" or country != "日本":
			theVisa = visaEntry.get()
			if theVisa != "KOKUSEKI" or theVisa != "kokuseki" or theVisa != "Kokuseki" or theVisa != "日本国籍" or theVisa != "国籍":
				if messagebox.askyesno("Visa Warning", "Have you verified the employee's right to work in Japan?"):
					completeRegistry()
		else:
			if messagebox.askyesno("Verify Japanese", "Have you confirmed the employee's Japanese citizenship?"):
				completeRegistry()




	##Window Arrangement - to be used multiple times

	cancelImage = PhotoImage(file="formtools/cancelButton.gif")
	checkImage = PhotoImage(file="formtools/checkButton.gif")
	randomImage = PhotoImage(file="formtools/randomButton.gif")
	regImage = PhotoImage(file="formtools/registerButton.gif")
	resetImage = PhotoImage(file="formtools/resetButton.gif")

	titleLabel = Label(rootWindow, font=mainFont, bg=bgColor, fg="white")
	titleLabel.place(x=125, y=36, anchor=CENTER)
	titleLabel.config(text=windowType)

	numberLabel = Label(rootWindow, text="Number", font=mainFont, bg=bgColor, fg="white") .place(x=102, y=80, anchor=CENTER)
	warningLabel = Label(rootWindow, text='For Japanese Nationals, enter "JAPAN" for country\nand "KOKUSEKI" for Visa', font=mainFont, bg=bgColor, fg="red", justify=CENTER)
	warningLabel.place(x=319, y=442, anchor=CENTER)

	lastLabel = Label(rootWindow, text="Last", font=mainFont, bg=bgColor, fg="white") .place(x=110, y=131, anchor=CENTER)
	firstLabel = Label(rootWindow, text="First", font=mainFont, bg=bgColor, fg="white") .place(x=368, y=131, anchor=CENTER)

	DOBLabel = Label(rootWindow, text="Date of Birth", font=mainFont, bg=bgColor, fg="white") .place(x=142, y=163, anchor=CENTER)
	homeLabel = Label(rootWindow, text="Home Country", font=mainFont, bg=bgColor, fg="white") .place(x=410, y=163, anchor=CENTER)

	yearLabel = Label(rootWindow, text="Year", font=mainFont, bg=bgColor, fg="gray").place(x=110, y=216, anchor=CENTER)
	monthLabel = Label(rootWindow, text="Month", font=mainFont, bg=bgColor, fg="gray") .place(x=204, y=216, anchor=CENTER)
	dayLabel = Label(rootWindow, text="Day", font=mainFont, bg=bgColor, fg="gray").place(x=288, y=216, anchor=CENTER)
	visaLabel = Label(rootWindow, text="Visa Status", font=mainFont, bg=bgColor, fg="white") .place(x=398, y=216, anchor=CENTER)

	phoneLabel = Label(rootWindow, text="Phone", font=mainFont, bg=bgColor, fg="white").place(x=118, y=245, anchor=CENTER)
	positionLabel = Label(rootWindow, text="Position", font=mainFont, bg=bgColor, fg="white").place(x=121, y=289, anchor=CENTER)
	wageLabel = Label(rootWindow, text="Hourly Wage", font=mainFont, bg=bgColor, fg="white").place(x=140, y=327, anchor=CENTER)
	statusLabel = Label(rootWindow, text="Employee Status", font=mainFont, bg=bgColor, fg="white") .place(x=453, y=293, anchor=CENTER)

	cancelButton = Button(rootWindow, image=cancelImage, command=killMe).place(x=516, y=376, anchor=CENTER)
	registerButton = Button(rootWindow, image=regImage, command=registerPress).place(x=403, y=376, anchor=CENTER)
	resetButton = Button(rootWindow, image=resetImage, command=resetEntries).place(x=121, y=376, anchor=CENTER)
	checkButton = Button(rootWindow, image=checkImage, command=checkNumber).place(x=293, y=79, anchor=CENTER)
	randomButton = Button(rootWindow, image=randomImage, command=getRandom).place(x=374, y=79, anchor=CENTER)

	numberEntry = Entry(rootWindow, bg=bgColor, fg="white", width=8, font=mainFont, justify=CENTER) 
	numberEntry.place(x=197, y=79, anchor=CENTER)

	lastEntry = Entry(rootWindow, bg=bgColor, fg="white", width=15, font=mainFont)
	lastEntry.place(x=229, y=128, anchor=CENTER)

	firstEntry = Entry(rootWindow, bg=bgColor, fg="white", width=14, font=mainFont)
	firstEntry.place(x=477, y=128, anchor=CENTER)

	YOBEntry = Entry(rootWindow, bg=bgColor, fg="white", width=6, font=mainFont, justify=CENTER)
	YOBEntry.place(x=127, y=187, anchor=CENTER)

	MOBEntry = Entry(rootWindow, bg=bgColor, fg="white", width=5, font=mainFont, justify=CENTER)
	MOBEntry.place(x=213, y=187, anchor=CENTER)

	DOBEntry = Entry(rootWindow, bg=bgColor, fg="white", width=5, font=mainFont, justify=CENTER)
	DOBEntry.place(x=293, y=187, anchor=CENTER)

	countryEntry = Entry(rootWindow, bg=bgColor, fg="white", width=18, font=mainFont)
	countryEntry.place(x=454, y=187, anchor=CENTER)

	visaEntry = Entry(rootWindow, bg=bgColor, fg="white", width=18, font=mainFont)
	visaEntry.place(x=454, y=243, anchor=CENTER)

	statusEntry = Entry(rootWindow, bg=bgColor, fg="white", width=18, font=mainFont, justify=CENTER)
	statusEntry.place(x=454, y=326, anchor=CENTER)

	phoneEntry = Entry(rootWindow, bg=bgColor, fg="white", width=15, font=mainFont)
	phoneEntry.place(x=236, y=243, anchor=CENTER)

	posEntry = Entry(rootWindow, bg=bgColor, fg="white", width=14, font=mainFont)
	posEntry.place(x=242, y=287, anchor=CENTER)

	wageEntry = Entry(rootWindow, bg=bgColor, fg="white", width=10, font=mainFont)
	wageEntry.place(x=257, y=326, anchor=CENTER)


	rootWindow.mainloop()




def getTC():
	global macFont
	global winFont
	macSize = "662x425"
	
	tc_keypad = None

	tc_keypad = Toplevel()
	tc_keypad.title("Time Clock")
	tc_keypad.geometry(macSize)
	tc_keypad.resizable(height=False, width=False)
	tc_keypad.configure(background="cornflower blue")

	statusLabel1 = "Please enter your Employee Number"
	statusLabel2 = 'Press "IN" or "OUT"'
	statusSummary = statusLabel1 + "\n" + statusLabel2
	
	hasDefaultSkin = True

	##Photo Array
	## Keypad
	photo = [PhotoImage(file="b0.gif"),
	PhotoImage(file="b1.gif"),
	PhotoImage(file="b2.gif"),
	PhotoImage(file="b3.gif"),
	PhotoImage(file="b4.gif"),
	PhotoImage(file="b5.gif"),
	PhotoImage(file="b6.gif"),
	PhotoImage(file="b7.gif"),
	PhotoImage(file="b8.gif"),
	PhotoImage(file="b9.gif"),
	PhotoImage(file="cButtonE.gif"), 
	PhotoImage(file="inButtonE.gif"), 
	PhotoImage(file="outButtonE.gif"), 
	PhotoImage(file="mgrButtonE.gif")] 

	LCDscreen = PhotoImage(file="LCDscreen.gif")
	keypadFrame = PhotoImage(file="keypadFrameBack.gif")
	backplate = PhotoImage(file="backPlate.gif")

	## Connect to the db created in the Employee Editor
	conn = sqlite3.connect('TestDB.db')
	c = conn.cursor()
	enforcer = conn.cursor()

	
	##Query Key
	queryKey = 0

	## App Massacure
	def killMe():
		messagebox.showinfo("Death", "Program will end.\nPress OK to FINISH HIM!")
		exit()

	## clear the display screen
	def clearEntry():
		global queryKey
		numberEntry.delete(0,END)
		queryKey=0


	## Insert numbers into the display screen
	def typeNumber(numberToType):
		numberEntry.insert(END, str(numberToType))


	def clockIn():
		global queryKey
		global statusLabel1, statusLabel2, statusSummary
		clockInTime = datetime.datetime.now()
		try:
			queryKey = int(numberEntry.get())
			## Entry syntax check
			if queryKey < 1000 or queryKey > 9999:
				thisStatusLabel.config(text="Entry rejected.\nPlease enter 4 digit number.", fg="red")
			else:
				c.execute("SELECT EmpID FROM EmpTable WHERE EmpID = " +str(queryKey))
				data = c.fetchone()
				if data == None:
					thisStatusLabel.config(text="Employee not found.", fg="red")
					clearEntry()
				else:
					c.execute("SELECT EmpID FROM TimeClock WHERE EmpID = "+str(queryKey) + " AND isClockedIn = 1")
					checkData = c.fetchone()
					if checkData != None:
						empName = c.execute("SELECT EmpLst FROM EmpTable WHERE EmpID = " +str(queryKey)).fetchall()[0][0]
						thisStatusLabel.config(text=empName+" already clocked in. \nAttempt rejected.", fg="red")
						clearEntry()
					else:
						empWage = c2.execute("SELECT EmpPay from EmpTable WHERE EmpID = "+str(queryKey)).fetchall()[0][0]
						c.execute("INSERT INTO TimeClock(ClockDate, EmpID, ClockIN, ClockOut, EmpPay, isClockedIn) VALUES (?, ?, ?, ?, ?, ?)", (clockInTime.strftime("%Y"+"%m"+"%d"), queryKey, clockInTime.strftime("%H"+"%M"), None, empWage, 1))
						empName = c.execute("SELECT EmpLst FROM EmpTable WHERE EmpID = " +str(queryKey)).fetchall()[0][0]
						statusLine1 = empName + " successful clock-in."
						statusLine2 = "In-Time: " + str(clockInTime.strftime("%H:%M"))
						thisStatusLabel.config(text=statusLine1+"\n"+statusLine2, fg="white")
						conn.commit()

		except ValueError:
			messagebox.showinfo("Empty Set", "Must enter a number to use these functions.")

		clearEntry()



	## Placeholder for Clock-Out Function
	def clockOut():
		global queryKey
		try:
			queryKey = int(numberEntry.get())
			clockOutTime = datetime.datetime.now()
			timeOut = clockOutTime.strftime("%H%M")
			dateOut = clockOutTime.strftime("%Y%m%d")
			## Entry syntax check
			if queryKey < 1000 or queryKey > 9999:
				thisStatusLabel.config(text="Entry rejected.\nPlease enter 4 digit number.", fg="red")
			else:
				c.execute("SELECT EmpID FROM TimeClock WHERE EmpID = " +str(queryKey) + " AND isClockedIn = 1")
				data = c.fetchone()
				if data == None:
					thisStatusLabel.config(text="Employee not found.", fg="red")
					clearEntry()
				else:
					startTime = c.execute("SELECT ClockIN FROM TimeClock WHERE EmpID = " + str(queryKey) + " AND isClockedIn = 1").fetchone()[0]
			
					if int(startTime) >= int(timeOut):
						midTime = 2400 - startTime
						HoursWorked = (midTime + int(timeOut)) / 100
					else: 
						HoursWorked = (int(timeOut) - startTime) / 100

					payRate = c.execute("SELECT EmpPay FROM EmpTable WHERE EmpID = " +str(queryKey)).fetchone()[0]

					c.execute("INSERT INTO Payroll(EmpID, ClockDate, HoursWorked, EmpPay, EntitledPay) VALUES (?,?,?,?,?)", (str(queryKey), int(c2.execute("SELECT ClockDate FROM TimeClock WHERE EmpID = " + str(queryKey) + " AND isClockedIn = 1").fetchone()[0]), HoursWorked, payRate, (payRate * HoursWorked)))

					c.execute("UPDATE TimeClock SET ClockOut = " +str(timeOut)+ " WHERE EmpID = " +str(queryKey))
					c.execute("UPDATE TimeClock SET isClockedIn = 0 WHERE EmpID = " +str(queryKey))

					empName = c.execute("SELECT EmpLst FROM EmpTable WHERE EmpID = " +str(queryKey)).fetchall()[0][0]

					thisStatusLabel.config(text=empName+" has been clocked out.\nClock-out time: " +str(clockOutTime.strftime("%H:%M")), fg="white")



					conn.commit()
					
		
		except ValueError:
			messagebox.showinfo("Empty Set", "Must enter a number to use these functions.")

		clearEntry()

	def openMenu():
		tc_keypad.destroy()

	## Display screen
	numberEntry = Entry(tc_keypad, font="none 40 bold", bg="dark sea green", fg="black", justify=CENTER, show="*", width=8)

	backPlate = Label(tc_keypad, image=backplate) .place(x=452, y=210, anchor=CENTER )

	##numeric key buttons
	numberButton = [ Button(tc_keypad, image=photo[0], command=lambda: typeNumber(0), bg="black"),
	Button(tc_keypad, image=photo[1], command=lambda: typeNumber(1), bg="black"),
	Button(tc_keypad, image=photo[2], command=lambda: typeNumber(2), bg="black"),
	Button(tc_keypad, image=photo[3], command=lambda: typeNumber(3), bg="black"),
	Button(tc_keypad, image=photo[4], command=lambda: typeNumber(4), bg="black"),
	Button(tc_keypad, image=photo[5], command=lambda: typeNumber(5), bg="black"),
	Button(tc_keypad, image=photo[6], command=lambda: typeNumber(6), bg="black"),
	Button(tc_keypad, image=photo[7], command=lambda: typeNumber(7), bg="black"),
	Button(tc_keypad, image=photo[8], command=lambda: typeNumber(8), bg="black"),
	Button(tc_keypad, image=photo[9], command=lambda: typeNumber(9), bg="black"),
	]

	##basic operations buttons
	clockInButton = Button(tc_keypad, image=photo[11], command=clockIn, bg="black")
	clockOutButton = Button(tc_keypad, image=photo[12], command=clockOut, bg="black")
	managerButton = Button(tc_keypad, image=photo[13], command=openMenu, bg="black")
	## Reset command for managerButton once manager menu is to be programmed
	clearButton = Button(tc_keypad, image=photo[10], command=clearEntry, bg="black")

	x1 = 329
	x2 = 456
	x3 = 579

	y1 = 68
	y2 = 163
	y3 = 258
	y4 = 352

	##Button Placements
	numberButton[0].place(x=x2, y=y4, anchor=CENTER)
	
	numberButton[1].place(x=x1, y=y1, anchor=CENTER)
	numberButton[2].place(x=x2, y=y1, anchor=CENTER)
	numberButton[3].place(x=x3, y=y1, anchor=CENTER)

	numberButton[4].place(x=x1, y=y2, anchor=CENTER)
	numberButton[5].place(x=x2, y=y2, anchor=CENTER)
	numberButton[6].place(x=x3, y=y2, anchor=CENTER)
	
	numberButton[7].place(x=x1, y=y3, anchor=CENTER)
	numberButton[8].place(x=x2, y=y3, anchor=CENTER)
	numberButton[9].place(x=x3, y=y3, anchor=CENTER)
	
		
	clearButton.place(x=x1, y=y4, anchor=CENTER)
	clockInButton.place(x=77, y=y4, anchor=CENTER)
	clockOutButton.place(x=197, y=y4, anchor=CENTER)

	managerButton.place(x=x3, y=y4, anchor=CENTER) ##Exit Button
	

	
	numberEntry.place(x=137, y=164, anchor=CENTER)

	thisStatusLabel = Label(tc_keypad, text=statusSummary, font=macFont, fg="white", bg="cornflower blue")
	thisStatusLabel.place(x=133, y=228, anchor=CENTER)

	## Program can only exit from the Manager Button
	## for DEV purposes, managerButton is our Kill Switch
	
	##Onscreen Clock and Calendar
	time1 = None
	def tick():
		global time1
		time1 = " "
		time2 = time.strftime('%H:%M:%S')
		if time2 != time1:
			time1 = time2
			clockDisplay.config(text=time2)
		clockDisplay.after(1000, tick)

	theDate = None
	def cal():
		global theDate
		theDate = " "
		date2 = datetime.datetime.now()
		if date2 != theDate:
			theDate = date2
			dateDisplay.config(text=date2.strftime("%b %d, %Y (%a)"))
		dateDisplay.after(1000, cal)


	clockDisplay = Label(tc_keypad, font="Helvetica 46 bold", bg="cornflower blue", fg="white", justify=CENTER)
	clockDisplay.place(x=132, y=75, anchor=CENTER)

	dateDisplay = Label(tc_keypad, font="Helvetica 18 bold", bg="cornflower blue", fg="white", justify=CENTER)
	dateDisplay.place(x=138, y=112, anchor=CENTER)



	tick()
	cal()
	tc_keypad.mainloop()


##Background Image
bg = Label(root, image=UIbg) .place(x=1, y=1, anchor=NW)

## Top and Bottom Banners
TopBanner = Label(root, image=UItopBanner) .place(x=1, y=1, anchor=NW)
## Running realtime clock and date display will go here in Column 1, and the About will go in Column 4
#$ Ads to go here in the FREE version
statusBanner = Label(root, image=UIstatus)
BottomBanner = Label(root, image=UIbottomBanner) .place(x=1, y=376, anchor=NW)
## Version and copyright information to go here

showEmpMenu = False
showDBMenu = False
showAdminMenu = False

## Opens and closes the button menu
def empMenu():
	global showEmpMenu, showdbMenu, showAdminMenu
	if showEmpMenu:
		showEmpMenu = False
		timeclockButton.place_forget()
		laborButton.place_forget()
		addEmpButton.place_forget()
	else:
		showEmpMenu = True
		timeclockButton.place(x=col1X, y=empY, anchor=NW)
		laborButton.place(x=col2X, y=empY, anchor=NW)
		addEmpButton.place(x=col3X, y=empY, anchor=NW)
		if showDBMenu:
			dbmenu()
		if showAdminMenu:
			adminMenu()

def dbmenu():
	global showEmpMenu, showDBMenu, showAdminMenu, editButton, payrollButton, searchButton
	if showDBMenu:
		showDBMenu = False
		editButton.place_forget()
		payrollButton.place_forget()
		searchButton.place_forget()
	else: 
		showDBMenu = True
		editButton.place(x=col1X, y=dbY, anchor=NW)
		payrollButton.place(x=col2X, y=dbY, anchor=NW)
		searchButton.place(x=col3X, y=dbY, anchor=NW)
		if showEmpMenu:
			empMenu()
		if showAdminMenu:
			adminMenu()

def adminMenu():
	global showAdminMenu, showEmpMenu, showDBMenu
	global settingsButton, aboutButton, exitButton
	if showAdminMenu:
		showAdminMenu = False
		settingsButton.place_forget()
		aboutButton.place_forget()
		exitButton.place_forget()
	else:
		showAdminMenu = True
		settingsButton.place(x=col1X, y=admY, anchor=NW)
		aboutButton.place(x=col2X, y=admY, anchor=NW)
		exitButton.place(x=col3X, y=admY, anchor=NW)
		if showEmpMenu:
			empMenu()
		if showDBMenu:
			dbmenu()



## Interactive Creations
employeeButton = Button(root, image=UIemployee, command=empMenu)
employeeButton.place(x=1, y=empY, anchor=NW)
## Submenu Button Instantiation
timeclockButton = Button(root, image=UItimeclock, command=getTC)
laborButton = Button(root, image=UIlabor, command=getCurrentLabor)
addEmpButton = Button(root, image=UIadd, command=addNew)


dbaseButton = Button(root, image=UIdbase, command=dbmenu)
dbaseButton.place(x=1, y=dbY, anchor=NW)
## Submenu Button Instantiation
editButton = Button(root, image=UIedit, command=None)
payrollButton = Button(root, image=UIpayroll, command=getPayroll)
searchButton = Button(root, image=UIsearch, command=None)


adminButton = Button(root, image=UIadmin, command=adminMenu)
adminButton.place(x=1, y=admY, anchor=NW)
## Submenu Button Instantiation
settingsButton = Button(root, image=UIsettings, command=None)
aboutButton = Button(root, image=UIAbout, command=getAbout)
exitButton = Button(root, image=UIexit, command=killMe)



## Live Clock
time1 = None
def tick():
	global time1, clockDisplay
	time1 = " "
	time2 = time.strftime('%H:%M:%S')
	if time2 != time1:
		time1 = time2
		clockDisplay.config(text=time2)
	clockDisplay.after(1000, tick)


clockDisplay = Label(root, font="Helvetica 35 bold", bg="blue", fg="White", width=8, justify=CENTER)
clockDisplay.place(x=12, y=8, anchor=NW)

## Live Calendar
date1 = None
def cal():
	global date1
	date1 = " "
	monthJ = None
	date2 = datetime.datetime.now()
	if date2 != date1:
		date1 = date2
		dateDisplay.config(text=date2.strftime('%b %d, %Y (%a)'))
	dateDisplay.after(1000, cal)


dateDisplay = Label(root, font="Helvetica 12 bold", bg="blue", fg="white", width=14, justify=CENTER)
dateDisplay.place(x=24, y=48, anchor=NW)

def clockedInCount():
	global clockedIn
	c.execute("SELECT Count(*) FROM TimeClock WHERE isClockedIn = 1")
	data = c.fetchone()[0]
	laborRate = [0]
	laborRate.append(c.execute("SELECT EmpPay FROM TimeClock WHERE isClockedIn = 1").fetchall())
	wages = [0]
	for i in range(data):
		wages.append(sum(laborRate[1][i]))

	payOut = "¥{:,}".format(sum(wages))+" per hour"
	statusLabel.config(text="Clocked In: " + str(data) + "   Labor Cost: "+payOut)
	dateDisplay.after(900, clockedInCount)


##run the clocks
tick()
cal()

titleCard = Label(root, text="TimeLord\nby Mitakasoft", font="Helvetica 18 bold", bg="blue", fg="white", width=10, justify=RIGHT)
titleCard.place(x=440, y=20, anchor=NW)

statusLabel = Label(root, font="Helvetica 12", bg="light gray", width=32, justify=LEFT)
statusLabel.place(x=9, y=385, anchor=NW)

copyrightLabel = Label(root, text="©︎2019 Mitakasoft.  Made in Japan", font="Helvetica 12", bg="light gray", width=25, justify=RIGHT) .place(x=380, y=385, anchor=NW)

clockedInCount()

##runs the program
root.mainloop()