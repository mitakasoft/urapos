from tkinter import *
from tkinter import messagebox
import datetime
import time
import sys
import os
import subprocess
import sqlite3

## The main manager menu - the initial program that will launch when the
## application is initially loaded

##Common coordinate variables
col1X = 144
col2X = 285
col3X = 428

empY = 74
dbY = 174
admY = 274

clockedIn = 0
laborCost = 0
laborLabel = "¥" + str(laborCost)



macSize = "572x415"
winSize = " "

##Set up for real-time receiving of labor data
conn = sqlite3.connect('TestDB.db')
c = conn.cursor()

root = Tk()
root.geometry(macSize)
root.title("TimeLord")
#root.resizable(width=False, height=False)


## UI Images
UIAbout = PhotoImage(file="ui/UIAbout.gif")
UIadmin = PhotoImage(file="ui/UIadmin.gif")
UIbg = PhotoImage(file="ui/UIbg.gif")
UIbottomBanner = PhotoImage(file="ui/UIbottomBanner.gif")
UIdbase = PhotoImage(file="ui/UIdbase.gif")
UIedit = PhotoImage(file="ui/UIedit.gif")
UIemployee = PhotoImage(file="ui/UIemployee.gif")
UIexit = PhotoImage(file="ui/UIexit.gif")
UIlabor = PhotoImage(file="ui/UIlabor.gif")
UIpayroll = PhotoImage(file="ui/UIpayroll.gif")
UIprint = PhotoImage(file="ui/UIprint.gif")
UIsearch = PhotoImage(file="ui/UIsearch.gif")
UIsettings = PhotoImage(file="ui/UIsettings.gif")
UIstatus = PhotoImage(file="ui/UIstatus.gif")
UItimeclock = PhotoImage(file="ui/UItimeclock.gif")
UItopBanner = PhotoImage(file="ui/UItopBanner.gif")



def killMe():
	if messagebox.askyesno("Confirm", "Are you sure you wish to quit?"):
		exit()

def getTC():
	macSize = "257x488"
	winSize = "300x500"

	tc_keypad = None

	tc_keypad = Toplevel()
	tc_keypad.title("Time Clock")
	tc_keypad.geometry(macSize)
	tc_keypad.resizable(height=False, width=False)

	bgImg = PhotoImage(file="metalBG.gif")
	bgLabel = Label(tc_keypad, image=bgImg)
	bgLabel.place(x=0, y=0, relwidth=1, relheight=1)

	hasDefaultSkin = True

	##Photo Array
	## Keypad
	photo = [PhotoImage(file="0button.gif"),
	PhotoImage(file="1button.gif"),
	PhotoImage(file="2button.gif"),
	PhotoImage(file="3button.gif"),
	PhotoImage(file="4button.gif"),
	PhotoImage(file="5button.gif"),
	PhotoImage(file="6button.gif"),
	PhotoImage(file="7button.gif"),
	PhotoImage(file="8button.gif"),
	PhotoImage(file="9button.gif"),
	PhotoImage(file="cancelbutton.gif"), 
	PhotoImage(file="inbutton.gif"), 
	PhotoImage(file="outbutton.gif"), 
	PhotoImage(file="mngrbutton.gif")] 

	## Connect to the db created in the Employee Editor
	conn = sqlite3.connect('TestDB.db')
	c = conn.cursor()

	c.execute("CREATE TABLE IF NOT EXISTS TimeClock(ClockDate NUMERIC, EmpID INTEGER, ClockIN NUMERIC, ClockOUT NUMERIC, isClockedIn NUMERIC)")

	##Query Key
	queryKey = 0

	## App Massacure
	def killMe():
		messagebox.showinfo("Death", "Program will end.\nPress OK to FINISH HIM!")
		exit()

	## clear the display screen
	def clearEntry():
		global queryKey
		numberEntry.delete(0,END)
		queryKey=0


	## Insert numbers into the display screen
	def typeNumber(numberToType):
		numberEntry.insert(END, str(numberToType))


	def clockIn():
		global queryKey
		clockInTime = datetime.datetime.now()
		try:
			queryKey = int(numberEntry.get())
			## Entry syntax check
			if queryKey < 1000 or queryKey > 9999:
				messagebox.showinfo("Invalid Entry", "Employee numbers are 4 digit.\nNo less than 1000.\nNo greater than 9999.\nPress OK to try again.")
			else:
				c.execute("SELECT EmpID FROM EmpTable WHERE EmpID = " +str(queryKey))
				data = c.fetchone()
				if data == None:
					messagebox.showerror("Invalid Entry", "There is no employee number " + str(queryKey)+". \nPlease recheck the number or register employee.")
					clearEntry()
				else:
					c.execute("SELECT EmpID FROM TimeClock WHERE EmpID = "+str(queryKey) + " AND isClockedIn = 1")
					checkData = c.fetchone()
					if checkData != None:
						messagebox.showwarning("Conflict", "Employee number "+str(queryKey)+" is already on the clock.\nPlease contact your manager for further details.")
						clearEntry()
					else:
						c.execute("INSERT INTO TimeClock(ClockDate, EmpID, ClockIN, ClockOut, isClockedIn) VALUES (?, ?, ?, ?, ?)", (clockInTime.strftime("%Y"+"%m"+"%d"), queryKey, clockInTime.strftime("%H"+"%M"), None, 1))
						messagebox.showinfo("Welcome", "Employee Number "+str(queryKey)+ " successfully clocked IN at " + str(clockInTime.strftime("%H" + ":" + "%M")))
						conn.commit()

		except ValueError:
			messagebox.showinfo("Empty Set", "Must enter a number to use these functions.")

		clearEntry()



	## Placeholder for Clock-Out Function
	def clockOut():
		global queryKey
		try:
			queryKey = int(numberEntry.get())
			clockOutTime = datetime.datetime.now()
			## Entry syntax check
			if queryKey < 1000 or queryKey > 9999:
				messagebox.showinfo("Invalid Entry", "Employee numbers are 4 digit.\nNo less than 1000.\nNo greater than 9999.\nPress OK to try again.")
			else:
				c.execute("SELECT EmpID FROM TimeClock WHERE EmpID = " +str(queryKey) + " AND isClockedIn = 1")
				data = c.fetchone()
				if data == None:
					messagebox.showerror("Invalid Entry", "There is no employee number " + str(queryKey) + "\nOR\nEmployee number " +str(queryKey) + " is not clocked in.")
					clearEntry()
				else:
					c.execute("UPDATE TimeClock SET ClockOut = " +clockOutTime.strftime("%H"+"%M")+ " WHERE EmpID = " +str(queryKey))
					c.execute("UPDATE TimeClock SET isClockedIn = 0 WHERE EmpID = " +str(queryKey))
					messagebox.showinfo("Have a Nice Day", "Employee Number "+str(queryKey)+ " successfully clocked OUT at " + str(clockOutTime.strftime("%H" + ":" + "%M")))
					conn.commit()
		except ValueError:
			messagebox.showinfo("Empty Set", "Must enter a number to use these functions.")

		clearEntry()

	def openMenu():
		tc_keypad.destroy()

	## Display screen
	numberEntry = Entry(tc_keypad, font="none 30 bold", bg="light green", fg="black", justify=CENTER, width=12, show="*")
	numberEntry.grid(column=1, row=2, columnspan=8, rowspan=2)

	##numeric key buttons
	numberButton = [ Button(tc_keypad, image=photo[0], command=lambda: typeNumber(0), bg="silver", font="none 30 italic"),
	Button(tc_keypad, image=photo[1], command=lambda: typeNumber(1), bg="silver", font="none 30 italic"),
	Button(tc_keypad, image=photo[2], command=lambda: typeNumber(2), bg="silver", font="none 30 italic"),
	Button(tc_keypad, image=photo[3], command=lambda: typeNumber(3), bg="silver", font="none 30 italic"),
	Button(tc_keypad, image=photo[4], command=lambda: typeNumber(4), bg="silver", font="none 30 italic"),
	Button(tc_keypad, image=photo[5], command=lambda: typeNumber(5), bg="silver", font="none 30 italic"),
	Button(tc_keypad, image=photo[6], command=lambda: typeNumber(6), bg="silver", font="none 30 italic"),
	Button(tc_keypad, image=photo[7], command=lambda: typeNumber(7), bg="silver", font="none 30 italic"),
	Button(tc_keypad, image=photo[8], command=lambda: typeNumber(8), bg="silver", font="none 30 italic"),
	Button(tc_keypad, image=photo[9], command=lambda: typeNumber(9), bg="silver", font="none 30 italic"),
	]

	##basic operations buttons
	clockInButton = Button(tc_keypad, image=photo[11], command=clockIn, bg="light green", font="none 20 bold")
	clockOutButton = Button(tc_keypad, image=photo[12], command=clockOut, bg="orange", font="none 20 bold")
	managerButton = Button(tc_keypad, image=photo[13], command=openMenu, bg="light blue", font="none 20 italic")
	## Reset command for managerButton once manager menu is to be programmed
	clearButton = Button(tc_keypad, image=photo[10], command=clearEntry, bg="silver", font="none 18 bold")




	##Button Placements
	numberButton[1].grid(column=3, row=6)
	numberButton[2].grid(column=4, row=6)
	numberButton[3].grid(column=5, row=6)
	numberButton[4].grid(column=3, row=7)
	numberButton[5].grid(column=4, row=7)
	numberButton[6].grid(column=5, row=7)
	numberButton[7].grid(column=3, row=8)
	clearButton.grid(column=3, row=9)
	numberButton[8].grid(column=4, row=8)
	numberButton[9].grid(column=5, row=8)

	numberButton[0].grid(column=4, row=9)
	clockInButton.grid(column=5, row=9)
	clockOutButton.grid(column=5, row=10)


	## Program can only exit from the Manager Button
	## for DEV purposes, managerButton is our Kill Switch
	managerButton.grid(column=4, row=10)



	##Onscreen Clock and Calendar
	time1 = None
	def tick():
		global time1
		time1 = " "
		time2 = time.strftime('%H:%M:%S')
		if time2 != time1:
			time1 = time2
			clockDisplay.config(text=time2)
		clockDisplay.after(1000, tick)

	theDate = None
	def cal():
		global theDate
		theDate = " "
		date2 = datetime.datetime.now()
		if date2 != theDate:
			theDate = date2
			dateDisplay.config(text=date2.strftime("%b %d, %Y (%a)"))
		dateDisplay.after(1000, cal)


	clockDisplay = Label(tc_keypad, font="Helvetica 32 bold", bg="black", fg="light green", justify=CENTER, width=11)
	clockDisplay.grid(column=1, row=12, columnspan=6, rowspan=1)

	dateDisplay = Label(tc_keypad, font="Helvetica 22 bold", bg="black", fg="light green", justify=CENTER, width=15)
	dateDisplay.grid(column=1, row=13, columnspan=6, rowspan=1)



	tick()
	cal()
	tc_keypad.mainloop()


##Background Image
bg = Label(root, image=UIbg) .place(x=1, y=1, anchor=NW)

## Top and Bottom Banners
TopBanner = Label(root, image=UItopBanner) .place(x=1, y=1, anchor=NW)
## Running realtime clock and date display will go here in Column 1, and the About will go in Column 4
#$ Ads to go here in the FREE version
statusBanner = Label(root, image=UIstatus)
BottomBanner = Label(root, image=UIbottomBanner) .place(x=1, y=376, anchor=NW)
## Version and copyright information to go here

showEmpMenu = False
showDBMenu = False
showAdminMenu = False

## Opens and closes the button menu
def empMenu():
	global showEmpMenu, showdbMenu, showAdminMenu
	if showEmpMenu:
		showEmpMenu = False
		timeclockButton.place_forget()
		laborButton.place_forget()
		printButton.place_forget()
	else:
		showEmpMenu = True
		timeclockButton.place(x=col1X, y=empY, anchor=NW)
		laborButton.place(x=col2X, y=empY, anchor=NW)
		printButton.place(x=col3X, y=empY, anchor=NW)
		if showDBMenu:
			dbmenu()
		if showAdminMenu:
			adminMenu()

def dbmenu():
	global showEmpMenu, showDBMenu, showAdminMenu, editButton, payrollButton, searchButton
	if showDBMenu:
		showDBMenu = False
		editButton.place_forget()
		payrollButton.place_forget()
		searchButton.place_forget()
	else: 
		showDBMenu = True
		editButton.place(x=col1X, y=dbY, anchor=NW)
		payrollButton.place(x=col2X, y=dbY, anchor=NW)
		searchButton.place(x=col3X, y=dbY, anchor=NW)
		if showEmpMenu:
			empMenu()
		if showAdminMenu:
			adminMenu()

def adminMenu():
	global showAdminMenu, showEmpMenu, showDBMenu
	global settingsButton, aboutButton, exitButton
	if showAdminMenu:
		showAdminMenu = False
		settingsButton.place_forget()
		aboutButton.place_forget()
		exitButton.place_forget()
	else:
		showAdminMenu = True
		settingsButton.place(x=col1X, y=admY, anchor=NW)
		aboutButton.place(x=col2X, y=admY, anchor=NW)
		exitButton.place(x=col3X, y=admY, anchor=NW)
		if showEmpMenu:
			empMenu()
		if showDBMenu:
			dbmenu()



## Interactive Creations
employeeButton = Button(root, image=UIemployee, command=empMenu)
employeeButton.place(x=1, y=empY, anchor=NW)
## Submenu Button Instantiation
timeclockButton = Button(root, image=UItimeclock, command=getTC)
laborButton = Button(root, image=UIlabor, command=None)
printButton = Button(root, image=UIprint, command=None)


dbaseButton = Button(root, image=UIdbase, command=dbmenu)
dbaseButton.place(x=1, y=dbY, anchor=NW)
## Submenu Button Instantiation
editButton = Button(root, image=UIedit, command=None)
payrollButton = Button(root, image=UIpayroll, command=None)
searchButton = Button(root, image=UIsearch, command=None)


adminButton = Button(root, image=UIadmin, command=adminMenu)
adminButton.place(x=1, y=admY, anchor=NW)
## Submenu Button Instantiation
settingsButton = Button(root, image=UIsettings, command=None)
aboutButton = Button(root, image=UIAbout, command=None)
exitButton = Button(root, image=UIexit, command=killMe)



## Live Clock
time1 = " "
def tick():
	global time1, clockDisplay
	time2 = time.strftime('%H:%M:%S')
	if time2 != time1:
		time1 = time2
		clockDisplay.config(text=time2)
	clockDisplay.after(1000, tick)


clockDisplay = Label(root, font="Helvetica 35 bold", bg="blue", fg="White", width=8, justify=CENTER)
clockDisplay.place(x=12, y=8, anchor=NW)

## Live Calendar
date1 = " "
def cal():
	global date1
	monthJ = None
	date2 = datetime.datetime.now()
	if date2 != date1:
		date1 = date2
		dateDisplay.config(text=date2.strftime('%b %d, %Y (%a)'))
	dateDisplay.after(1000, cal)


dateDisplay = Label(root, font="Helvetica 12 bold", bg="blue", fg="white", width=14, justify=CENTER)
dateDisplay.place(x=24, y=48, anchor=NW)

def clockedInCount():
	global clockedIn
	c.execute("SELECT Count(*) FROM TimeClock WHERE isClockedIn = 1")
	data = c.fetchone()
	statusLabel.config(text="Clocked In: " + str(data) + "   Labor Cost: " + laborLabel)
	dateDisplay.after(900, clockedInCount)


##run the clocks
tick()
cal()

titleCard = Label(root, text="TimeLord\nby Mitakasoft", font="Helvetica 18 bold", bg="blue", fg="white", width=10, justify=RIGHT)
titleCard.place(x=440, y=20, anchor=NW)

statusLabel = Label(root, font="Helvetica 12", bg="light gray", width=25, justify=LEFT)
statusLabel.place(x=9, y=385, anchor=NW)

copyrightLabel = Label(root, text="©︎2019 Mitakasoft.  Made in Japan", font="Helvetica 12", bg="light gray", width=25, justify=RIGHT) .place(x=380, y=385, anchor=NW)

clockedInCount()

##runs the program
root.mainloop()