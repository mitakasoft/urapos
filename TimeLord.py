from tkinter import *
from tkinter import messagebox
from tkinter import ttk
import datetime
import time
import sys
import os
import subprocess
import sqlite3
import random

## The main manager menu - the initial program that will launch when the
## application is initially loaded

##IMPORTANT: Readjusted measurements in the code, re-coded in WINDOWS
##HUGE Measurement discrepencies between Mac and Windows versions
##One version does NOT fit all, and will have to make minor adjustments
##between the platforms.

##THIS one is Windows Optimized

##Common coordinate variables
col1X = 144
col2X = 285
col3X = 428

empY = 74
dbY = 174
admY = 274

clockedIn = 0
laborCost = 0
laborLabel = "¥" + str(laborCost)

customerName = "Licensee Name"

bgColor = '#253340'
borderGray = '#757272'
mainFont = "Helvetica 18"

macSize = "572x415"

##Set up for real-time receiving of labor data
conn = sqlite3.connect('TestDB.db')
c = conn.cursor()
c2 = conn.cursor()

c.execute("CREATE TABLE IF NOT EXISTS TimeClock(ClockDate NUMERIC, EmpID INTEGER, ClockIN NUMERIC, ClockOUT NUMERIC, EmpPay NUMERIC, isClockedIn NUMERIC)")


root = Tk()
root.geometry(macSize)
root.title("TimeLord")
root.resizable(width=False, height=False)


## UI Images
UIAbout = PhotoImage(file="ui/UIAbout.gif")
UIadmin = PhotoImage(file="ui/UIadmin.gif")
UIbg = PhotoImage(file="ui/UIbg.gif")
UIbottomBanner = PhotoImage(file="ui/UIbottomBanner.gif")
UIdbase = PhotoImage(file="ui/UIdbase.gif")
UIedit = PhotoImage(file="ui/UIedit.gif")
UIemployee = PhotoImage(file="ui/UIemployee.gif")
UIexit = PhotoImage(file="ui/UIexit.gif")
UIlabor = PhotoImage(file="ui/UIlabor.gif")
UIpayroll = PhotoImage(file="ui/UIpayroll.gif")
UIadd = PhotoImage(file="ui/HR.gif")
UIsearch = PhotoImage(file="ui/UIsearch.gif")
UIsettings = PhotoImage(file="ui/UIsettings.gif")
UIstatus = PhotoImage(file="ui/UIstatus.gif")
UItimeclock = PhotoImage(file="ui/UItimeclock.gif")
UItopBanner = PhotoImage(file="ui/UItopBanner.gif")



def killMe():
	if messagebox.askyesno("Confirm", "Are you sure you wish to quit?"):
		exit()

def getCurrentLabor():
	count = c.execute("SELECT COUNT(*) FROM TimeClock WHERE isClockedIn = 1").fetchall()[0][0]

	if count == 0:
		messagebox.showinfo("Empty", "No one is currently clocked-in.")

	else:
		laborView = Toplevel()
		laborView.title("Current Labor")
		laborView.geometry("640x480")
		laborView.resizable(width=False, height=False)

		_exitButton = PhotoImage(file="formtools/exitButton.gif")
		bgColor = "#34bfd4"

		spread = ttk.Treeview(laborView)

		laborView.config(bg=bgColor)

		def killMe(event):
			laborView.destroy()

		def killMeNow():
			laborView.destroy()

		laborView.bind("<Escape>", killMe)

		titleLabel = Label(laborView, text="Current Labor View", font="Helvetica 28 bold", fg="black", bg=bgColor, justify=CENTER).place(x=310,y=50,anchor=CENTER)

		spread["columns"] = (0,1,2,3)
		spread.column(0, width=80)
		spread.column(1, width=50)
		spread.column(2, width=50)
		spread.column(3, width=50)


		spread.heading(0, text = "ClockDate")
		spread.heading(1, text = "EmpID")
		spread.heading(2, text = "In Time")
		spread.heading(3, text = "Pay Rate")

		spread.place(x=312, y=199, anchor=CENTER)


		exitButton = Button(laborView, image=_exitButton, command=killMeNow).place(x=320, y=415, anchor=CENTER)

		
		data = c.execute("SELECT * FROM TimeClock WHERE isClockedIn = 1").fetchall()

		lastname = []
		firstname = []

		for i in range(0, count):
			##Read the names from the EmpTable where the ID numbers in TimeClock are ClockedIn
			##Take those names and return them to the propper array
			##output them in the aforementioned forloop.
			lastname.append(c.execute("SELECT EmpLst FROM EmpTable WHERE EmpID = " +str(data[i][1])).fetchall()[0][0])
			firstname.append(c.execute("SELECT EmpFst FROM EmpTable WHERE EmpID = " +str(data[i][1])).fetchall()[0][0])

		clockIn = []
		ID = []
		inTime = []
		payOut = []

		for i in range(0, count):
			clockIn.append(data[i][0])
			ID.append(data[i][1])
			inTime.append(data[i][2])
			payOut.append(data[i][4])
			spread.insert("", i, text=lastname[i]+" "+firstname[i], values=(clockIn[i],ID[i], inTime[i], payOut[i]))


		laborView.mainloop()




def getAbout():
	aboutWindow = Toplevel()
	aboutWindow.title("About: TimeLord")
	aboutWindow.resizable(height=False, width=False)
	aboutWindow.geometry("640x480")


	def killMe():
		aboutWindow.destroy()

	aboutLogo = PhotoImage(file="aboutLogo.gif")
	aboutTrim = PhotoImage(file="aboutTrim.gif")

	trimLabel = Label(aboutWindow, image=aboutTrim) .place(x=0, y=0)
	titleLabel = Label(aboutWindow, text="TimeLord®︎", bg="white", font="Helvetica 35 bold") .place(x=25, y=3, anchor=NW)
	byLineLabel = Label(aboutWindow, text="by Mitakasoft", font="Helvetica 10", bg="white", justify=RIGHT) .place(x=180, y=65, anchor=NW)
	versionLabel = Label(aboutWindow, text="Version Number: 0.ALPHA", font="Helvetica 12", bg="white", justify=RIGHT) .place(x=435, y=74, anchor=CENTER)

	aboutIcon = Label(aboutWindow, image=aboutLogo) .place(x=14, y=120, anchor=NW)

	legal1 = Label(aboutWindow, text="©︎2019 Mitakasoft\nNishitokyo, Tokyo, Japan\nhttps://mitakasoft.jp", bg="white", font="helvetica 12", justify=LEFT) .place(x=29, y=355, anchor=NW)

	licenseIntro = Label(aboutWindow, text="This software is\nlicensed to:", font="Helvetica 12", bg="white", justify=LEFT) .place(x=227, y=126, anchor=NW)
	licenseeName = Label(aboutWindow, text=customerName, font="Helvetica 12",bg="white",justify=RIGHT)  ##Editable so must be instantiated independently
	licenseeName.place(x=533, y=146, anchor=NE)

	licenseeAddyIntro = Label(aboutWindow, text="Licensee Address:", font="Helvetica 12", bg="white",justify=LEFT) .place(x=227, y=183, anchor=NW)
	licenseeAddy = Label(aboutWindow, text="1-2-3 Dokodemo\nMinato-Ku, Tokyo\n000-0000 Japan", font="Helvetica 12",bg="white", justify=RIGHT)
	licenseeAddy.place(x=533, y=184, anchor=NE)

	legal2 = Label(aboutWindow, text="For exclusive use by the licensee\nonly.  Unauthorized use is strictly\nprohibited by law.", font="Helvetica 12", bg="white",justify=RIGHT) .place(x=533, y=303, anchor=E)

	credit = Label(aboutWindow, text="Built and created by\nMichael Yamazaki\nMade in Japan", font="Helvetica 12", bg="white",justify=RIGHT) .place(x=533, y=389, anchor=E)

	deathButton = Button(aboutWindow, text="   Close   ", command=killMe)
	deathButton.place(x=269, y=439, anchor=NW)

	aboutWindow.mainloop()



def addNew():
	windowType = "Create Employee"

	bgColor = '#253340'
	borderGray = '#757272'
	mainFont = "Helvetica 14"

	rootWindow = Toplevel()
	rootWindow.geometry("640x480")
	rootWindow.resizable(width=False, height=False)
	rootWindow.title(windowType)

	bg = PhotoImage(file="formtools/background.gif")
	bgImage = Label(rootWindow, image=bg).place(x=-3, y=-3)

	##REMOVE FOR INTEGRATION##
	
	def killMe():
		rootWindow.destroy()

	def destroyMe(event):
		killMe()

	rootWindow.bind("<Escape>", destroyMe)

	def resetEntries():
		if messagebox.askyesno("Confirm", "Are you sure you want to reset the form?"):
			numberEntry.delete(0,END)
			lastEntry.delete(0,END)
			firstEntry.delete(0,END)
			YOBEntry.delete(0,END)
			MOBEntry.delete(0,END)
			DOBEntry.delete(0,END)
			countryEntry.delete(0,END)
			phoneEntry.delete(0,END)
			visaEntry.delete(0,END)
			posEntry.delete(0,END)
			wageEntry.delete(0,END)
			statusEntry.delete(0,END)

	def hardReset():
		numberEntry.delete(0,END)
		lastEntry.delete(0,END)
		firstEntry.delete(0,END)
		YOBEntry.delete(0,END)
		MOBEntry.delete(0,END)
		DOBEntry.delete(0,END)
		countryEntry.delete(0,END)
		phoneEntry.delete(0,END)
		visaEntry.delete(0,END)
		posEntry.delete(0,END)
		wageEntry.delete(0,END)
		statusEntry.delete(0,END)


	def checkNumber():
		try:
			verify = numberEntry.get()
			if int(verify) < 999 or int(verify) > 9999:
				messagebox.showerror("Invalid Entry", "Numbers cannot be less than 1000 or greater than 9999, and must contain 4 digits.")
			else:
				checkNumber = c.execute("SELECT Count(*) FROM EmpTable WHERE EmpID = " +str(verify)).fetchone()[0]
				if checkNumber >= 1:
					messagebox.showerror("Conflict", str(verify)+" already exists in the database.  Please issue a different number or randomize.")
				else:
					messagebox.showwarning("It's Good", str(verify)+" is good to use.")
		except ValueError:
			messagebox.showerror("Nothing Entered", "You must enter a number in this field.")

	def getRandom():
		numberEntry.delete(0,END)
		randEntry = random.randrange(1000, 9999)
		verify = c.execute("SELECT Count(*) FROM EmpTable WHERE EmpID = " +str(randEntry)).fetchone()[0]
		while verify >= 1:
			randEntry = random.randrange(1000, 9999)
			reverify = c.execute("SELECT COUNT (*) FROM EmpTable WHERE EmpID = " + str(randEntry)).fetchone()[0]
		numberEntry.insert(0, str(randEntry))

	def registerPress():
		try:
			checkNumber()
			checkCountry()
		except ValueError:
			messagebox.showerror("Incomplete Input", "There are blank fields in the form.  Make sure all information is completely filled in before submission.")

	def completeRegistry():
		try:
			ID = numberEntry.get()
			lastName = lastEntry.get()
			firstName = firstEntry.get()
			YOB = int(YOBEntry.get())
			MOB = int(MOBEntry.get())
			if MOB < 1 or MOB > 12:
				messagebox.showerror("Calendar Error", "There are only 12 months in a year.  Please enter 1 through 12 to indicate the month.")
			else:
				DOB = int(DOBEntry.get())
				if DOB < 1 or DOB > 32:
					messagebox.showerror("Calendar Error", "Months with 30 days: April (4), June (6), September (9), November (11).\nIrregular Month: February (2)\nAll other months: 31 Days.\nNo month has "+str(DOB)+" days.")
				else:
					phoneNumber = phoneEntry.get()
					country = countryEntry.get()
					theVisa = visaEntry.get()
					position = posEntry.get()
					pay = wageEntry.get()
					empStatus = statusEntry.get()

					if messagebox.askyesno("Ready to Register", "Would you like to register " + lastName+", "+firstName+"?"):
						c.execute("INSERT INTO EmpTable(EmpID, EmpLst, EmpFst, EmpYOB, EmpMOB, EmpDOB, EmpPho, EmpCou, EmpVsa, EmpPos, EmpPay, EmpSts) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
							(ID, lastName, firstName, YOB, MOB, DOB, phoneNumber, country, theVisa, position, pay, empStatus))
						conn.commit()
						messagebox.showinfo("Entry Successful", lastName+", "+firstName+"'s registry was completed successfully.")
						hardReset()
					else:
						messagebox.showwarning("Aborted", "Submission cancelled.")
		except ValueError:
			messagebox.showerror("Error", "Some data was invalid or incomplete.\nPlease verify and resubmit the form.")



	def checkCountry():
		country = countryEntry.get()
		if country != "JAPAN" or country!= "Japan" or country != "japan" or country != "日本":
			theVisa = visaEntry.get()
			if theVisa != "KOKUSEKI" or theVisa != "kokuseki" or theVisa != "Kokuseki" or theVisa != "日本国籍" or theVisa != "国籍":
				if messagebox.askyesno("Visa Warning", "Have you verified the employee's right to work in Japan?"):
					completeRegistry()
		else:
			if messagebox.askyesno("Verify Japanese", "Have you confirmed the employee's Japanese citizenship?"):
				completeRegistry()




	##Window Arrangement - to be used multiple times

	cancelImage = PhotoImage(file="formtools/cancelButton.gif")
	checkImage = PhotoImage(file="formtools/checkButton.gif")
	randomImage = PhotoImage(file="formtools/randomButton.gif")
	regImage = PhotoImage(file="formtools/registerButton.gif")
	resetImage = PhotoImage(file="formtools/resetButton.gif")

	titleLabel = Label(rootWindow, font=mainFont, bg=bgColor, fg="white")
	titleLabel.place(x=125, y=36, anchor=CENTER)
	titleLabel.config(text=windowType)

	numberLabel = Label(rootWindow, text="Number", font=mainFont, bg=bgColor, fg="white") .place(x=102, y=80, anchor=CENTER)
	warningLabel = Label(rootWindow, text='For Japanese Nationals, enter "JAPAN" for country\nand "KOKUSEKI" for Visa', font=mainFont, bg=bgColor, fg="red", justify=CENTER)
	warningLabel.place(x=319, y=442, anchor=CENTER)

	lastLabel = Label(rootWindow, text="Last", font=mainFont, bg=bgColor, fg="white") .place(x=110, y=131, anchor=CENTER)
	firstLabel = Label(rootWindow, text="First", font=mainFont, bg=bgColor, fg="white") .place(x=368, y=131, anchor=CENTER)

	DOBLabel = Label(rootWindow, text="Date of Birth", font=mainFont, bg=bgColor, fg="white") .place(x=142, y=163, anchor=CENTER)
	homeLabel = Label(rootWindow, text="Home Country", font=mainFont, bg=bgColor, fg="white") .place(x=410, y=163, anchor=CENTER)

	yearLabel = Label(rootWindow, text="Year", font=mainFont, bg=bgColor, fg="gray").place(x=110, y=216, anchor=CENTER)
	monthLabel = Label(rootWindow, text="Month", font=mainFont, bg=bgColor, fg="gray") .place(x=204, y=216, anchor=CENTER)
	dayLabel = Label(rootWindow, text="Day", font=mainFont, bg=bgColor, fg="gray").place(x=288, y=216, anchor=CENTER)
	visaLabel = Label(rootWindow, text="Visa Status", font=mainFont, bg=bgColor, fg="white") .place(x=398, y=216, anchor=CENTER)

	phoneLabel = Label(rootWindow, text="Phone", font=mainFont, bg=bgColor, fg="white").place(x=118, y=245, anchor=CENTER)
	positionLabel = Label(rootWindow, text="Position", font=mainFont, bg=bgColor, fg="white").place(x=121, y=289, anchor=CENTER)
	wageLabel = Label(rootWindow, text="Hourly Wage", font=mainFont, bg=bgColor, fg="white").place(x=140, y=327, anchor=CENTER)
	statusLabel = Label(rootWindow, text="Employee Status", font=mainFont, bg=bgColor, fg="white") .place(x=453, y=293, anchor=CENTER)

	cancelButton = Button(rootWindow, image=cancelImage, command=killMe).place(x=516, y=376, anchor=CENTER)
	registerButton = Button(rootWindow, image=regImage, command=registerPress).place(x=403, y=376, anchor=CENTER)
	resetButton = Button(rootWindow, image=resetImage, command=resetEntries).place(x=121, y=376, anchor=CENTER)
	checkButton = Button(rootWindow, image=checkImage, command=checkNumber).place(x=293, y=79, anchor=CENTER)
	randomButton = Button(rootWindow, image=randomImage, command=getRandom).place(x=374, y=79, anchor=CENTER)

	numberEntry = Entry(rootWindow, bg=bgColor, fg="white", width=8, font=mainFont, justify=CENTER) 
	numberEntry.place(x=197, y=79, anchor=CENTER)

	lastEntry = Entry(rootWindow, bg=bgColor, fg="white", width=15, font=mainFont)
	lastEntry.place(x=229, y=128, anchor=CENTER)

	firstEntry = Entry(rootWindow, bg=bgColor, fg="white", width=14, font=mainFont)
	firstEntry.place(x=477, y=128, anchor=CENTER)

	YOBEntry = Entry(rootWindow, bg=bgColor, fg="white", width=6, font=mainFont, justify=CENTER)
	YOBEntry.place(x=127, y=187, anchor=CENTER)

	MOBEntry = Entry(rootWindow, bg=bgColor, fg="white", width=5, font=mainFont, justify=CENTER)
	MOBEntry.place(x=213, y=187, anchor=CENTER)

	DOBEntry = Entry(rootWindow, bg=bgColor, fg="white", width=5, font=mainFont, justify=CENTER)
	DOBEntry.place(x=293, y=187, anchor=CENTER)

	countryEntry = Entry(rootWindow, bg=bgColor, fg="white", width=18, font=mainFont)
	countryEntry.place(x=454, y=187, anchor=CENTER)

	visaEntry = Entry(rootWindow, bg=bgColor, fg="white", width=18, font=mainFont)
	visaEntry.place(x=454, y=243, anchor=CENTER)

	statusEntry = Entry(rootWindow, bg=bgColor, fg="white", width=18, font=mainFont, justify=CENTER)
	statusEntry.place(x=454, y=326, anchor=CENTER)

	phoneEntry = Entry(rootWindow, bg=bgColor, fg="white", width=15, font=mainFont)
	phoneEntry.place(x=236, y=243, anchor=CENTER)

	posEntry = Entry(rootWindow, bg=bgColor, fg="white", width=14, font=mainFont)
	posEntry.place(x=242, y=287, anchor=CENTER)

	wageEntry = Entry(rootWindow, bg=bgColor, fg="white", width=10, font=mainFont)
	wageEntry.place(x=257, y=326, anchor=CENTER)





	rootWindow.mainloop()




def getTC():
	macSize = "440x766"
	
	tc_keypad = None

	tc_keypad = Toplevel()
	tc_keypad.title("Time Clock")
	tc_keypad.geometry(macSize)
	tc_keypad.resizable(height=False, width=False)
	tc_keypad.configure(background="cornflower blue")

	
	hasDefaultSkin = True

	##Photo Array
	## Keypad
	photo = [PhotoImage(file="b0.gif"),
	PhotoImage(file="b1.gif"),
	PhotoImage(file="b2.gif"),
	PhotoImage(file="b3.gif"),
	PhotoImage(file="b4.gif"),
	PhotoImage(file="b5.gif"),
	PhotoImage(file="b6.gif"),
	PhotoImage(file="b7.gif"),
	PhotoImage(file="b8.gif"),
	PhotoImage(file="b9.gif"),
	PhotoImage(file="cButtonE.gif"), 
	PhotoImage(file="inButtonE.gif"), 
	PhotoImage(file="outButtonE.gif"), 
	PhotoImage(file="mgrButtonE.gif")] 

	LCDscreen = PhotoImage(file="LCDscreen.gif")
	keypadFrame = PhotoImage(file="keypadFrameBack.gif")
	detailIcon = PhotoImage(file="keypadIcon.gif")

	## Connect to the db created in the Employee Editor
	conn = sqlite3.connect('TestDB.db')
	c = conn.cursor()
	enforcer = conn.cursor()

	
	##Query Key
	queryKey = 0

	## App Massacure
	def killMe():
		messagebox.showinfo("Death", "Program will end.\nPress OK to FINISH HIM!")
		exit()

	## clear the display screen
	def clearEntry():
		global queryKey
		numberEntry.delete(0,END)
		queryKey=0


	## Insert numbers into the display screen
	def typeNumber(numberToType):
		numberEntry.insert(END, str(numberToType))


	def clockIn():
		global queryKey
		clockInTime = datetime.datetime.now()
		try:
			queryKey = int(numberEntry.get())
			## Entry syntax check
			if queryKey < 1000 or queryKey > 9999:
				messagebox.showinfo("Invalid Entry", "Employee numbers are 4 digit.\nNo less than 1000.\nNo greater than 9999.\nPress OK to try again.")
			else:
				c.execute("SELECT EmpID FROM EmpTable WHERE EmpID = " +str(queryKey))
				data = c.fetchone()
				if data == None:
					messagebox.showerror("Invalid Entry", "There is no employee number " + str(queryKey)+". \nPlease recheck the number or register employee.")
					clearEntry()
				else:
					c.execute("SELECT EmpID FROM TimeClock WHERE EmpID = "+str(queryKey) + " AND isClockedIn = 1")
					checkData = c.fetchone()
					if checkData != None:
						messagebox.showwarning("Conflict", "Employee number "+str(queryKey)+" is already on the clock.\nPlease contact your manager for further details.")
						clearEntry()
					else:
						empWage = c2.execute("SELECT EmpPay from EmpTable WHERE EmpID = "+str(queryKey)).fetchall()[0][0]
						c.execute("INSERT INTO TimeClock(ClockDate, EmpID, ClockIN, ClockOut, EmpPay, isClockedIn) VALUES (?, ?, ?, ?, ?, ?)", (clockInTime.strftime("%Y"+"%m"+"%d"), queryKey, clockInTime.strftime("%H"+"%M"), None, empWage, 1))
						messagebox.showinfo("Welcome", "Employee Number "+str(queryKey)+ " successfully clocked IN at " + str(clockInTime.strftime("%H" + ":" + "%M")))
						conn.commit()

		except ValueError:
			messagebox.showinfo("Empty Set", "Must enter a number to use these functions.")

		clearEntry()



	## Placeholder for Clock-Out Function
	def clockOut():
		global queryKey
		try:
			queryKey = int(numberEntry.get())
			clockOutTime = datetime.datetime.now()
			## Entry syntax check
			if queryKey < 1000 or queryKey > 9999:
				messagebox.showinfo("Invalid Entry", "Employee numbers are 4 digit.\nNo less than 1000.\nNo greater than 9999.\nPress OK to try again.")
			else:
				c.execute("SELECT EmpID FROM TimeClock WHERE EmpID = " +str(queryKey) + " AND isClockedIn = 1")
				data = c.fetchone()
				if data == None:
					messagebox.showerror("Invalid Entry", "There is no employee number " + str(queryKey) + "\nOR\nEmployee number " +str(queryKey) + " is not clocked in.")
					clearEntry()
				else:
					c.execute("UPDATE TimeClock SET ClockOut = " +clockOutTime.strftime("%H"+"%M")+ " WHERE EmpID = " +str(queryKey))
					c.execute("UPDATE TimeClock SET isClockedIn = 0 WHERE EmpID = " +str(queryKey))
					messagebox.showinfo("Have a Nice Day", "Employee Number "+str(queryKey)+ " successfully clocked OUT at " + str(clockOutTime.strftime("%H" + ":" + "%M")))
					conn.commit()
		except ValueError:
			messagebox.showinfo("Empty Set", "Must enter a number to use these functions.")

		clearEntry()

	def openMenu():
		tc_keypad.destroy()

	## Display screen
	numberEntry = Entry(tc_keypad, font="none 48 bold", bg="dark sea green", fg="black", justify=CENTER, show="*", width=11)

	
	##numeric key buttons
	numberButton = [ Button(tc_keypad, image=photo[0], command=lambda: typeNumber(0), bg="black"),
	Button(tc_keypad, image=photo[1], command=lambda: typeNumber(1), bg="black"),
	Button(tc_keypad, image=photo[2], command=lambda: typeNumber(2), bg="black"),
	Button(tc_keypad, image=photo[3], command=lambda: typeNumber(3), bg="black"),
	Button(tc_keypad, image=photo[4], command=lambda: typeNumber(4), bg="black"),
	Button(tc_keypad, image=photo[5], command=lambda: typeNumber(5), bg="black"),
	Button(tc_keypad, image=photo[6], command=lambda: typeNumber(6), bg="black"),
	Button(tc_keypad, image=photo[7], command=lambda: typeNumber(7), bg="black"),
	Button(tc_keypad, image=photo[8], command=lambda: typeNumber(8), bg="black"),
	Button(tc_keypad, image=photo[9], command=lambda: typeNumber(9), bg="black"),
	]

	##basic operations buttons
	clockInButton = Button(tc_keypad, image=photo[11], command=clockIn, bg="black")
	clockOutButton = Button(tc_keypad, image=photo[12], command=clockOut, bg="black")
	managerButton = Button(tc_keypad, image=photo[13], command=openMenu, bg="black")
	## Reset command for managerButton once manager menu is to be programmed
	clearButton = Button(tc_keypad, image=photo[10], command=clearEntry, bg="black")



	##Button Placements
	numberButton[0].place(x=163, y=570, anchor=NW)
	
	numberButton[1].place(x=48, y=314, anchor=NW)
	numberButton[2].place(x=163, y=314, anchor=NW)
	numberButton[3].place(x=275, y=314, anchor=NW)

	numberButton[4].place(x=48, y=399, anchor=NW)
	numberButton[5].place(x=163, y=399, anchor=NW)
	numberButton[6].place(x=275, y=399, anchor=NW)
	
	numberButton[7].place(x=48, y=485, anchor=NW)
	numberButton[8].place(x=163, y=485, anchor=NW)
	numberButton[9].place(x=275, y=485, anchor=NW)
	
	keypadIconLabel = Label(tc_keypad, image=detailIcon) .place(x=160, y=654, anchor=NW)
	
	clearButton.place(x=48, y=570, anchor=NW)
	clockInButton.place(x=275, y=570, anchor=NW)
	clockOutButton.place(x=275, y=654, anchor=NW)

	managerButton.place(x=48, y=654, anchor=NW)
	

	#fakeLCDscreen = Label(tc_keypad, image=LCDscreen) .place(x=43, y=198, anchor=NW)
	
	numberEntry.place(x=20, y=198, anchor=NW)

	## Program can only exit from the Manager Button
	## for DEV purposes, managerButton is our Kill Switch
	
	##Onscreen Clock and Calendar
	time1 = None
	def tick():
		global time1
		time1 = " "
		time2 = time.strftime('%H:%M:%S')
		if time2 != time1:
			time1 = time2
			clockDisplay.config(text=time2)
		clockDisplay.after(1000, tick)

	theDate = None
	def cal():
		global theDate
		theDate = " "
		date2 = datetime.datetime.now()
		if date2 != theDate:
			theDate = date2
			dateDisplay.config(text=date2.strftime("%b %d, %Y (%a)"))
		dateDisplay.after(1000, cal)


	clockDisplay = Label(tc_keypad, font="Helvetica 75 bold", bg="cornflower blue", fg="white", justify=CENTER)
	clockDisplay.place(x=15, y=26, anchor=NW)

	dateDisplay = Label(tc_keypad, font="Helvetica 24 bold", bg="cornflower blue", fg="white", justify=CENTER)
	dateDisplay.place(x=80, y=131, anchor=NW)



	tick()
	cal()
	tc_keypad.mainloop()


##Background Image
bg = Label(root, image=UIbg) .place(x=1, y=1, anchor=NW)

## Top and Bottom Banners
TopBanner = Label(root, image=UItopBanner) .place(x=1, y=1, anchor=NW)
## Running realtime clock and date display will go here in Column 1, and the About will go in Column 4
#$ Ads to go here in the FREE version
statusBanner = Label(root, image=UIstatus)
BottomBanner = Label(root, image=UIbottomBanner) .place(x=1, y=376, anchor=NW)
## Version and copyright information to go here

showEmpMenu = False
showDBMenu = False
showAdminMenu = False

## Opens and closes the button menu
def empMenu():
	global showEmpMenu, showdbMenu, showAdminMenu
	if showEmpMenu:
		showEmpMenu = False
		timeclockButton.place_forget()
		laborButton.place_forget()
		addEmpButton.place_forget()
	else:
		showEmpMenu = True
		timeclockButton.place(x=col1X, y=empY, anchor=NW)
		laborButton.place(x=col2X, y=empY, anchor=NW)
		addEmpButton.place(x=col3X, y=empY, anchor=NW)
		if showDBMenu:
			dbmenu()
		if showAdminMenu:
			adminMenu()

def dbmenu():
	global showEmpMenu, showDBMenu, showAdminMenu, editButton, payrollButton, searchButton
	if showDBMenu:
		showDBMenu = False
		editButton.place_forget()
		payrollButton.place_forget()
		searchButton.place_forget()
	else: 
		showDBMenu = True
		editButton.place(x=col1X, y=dbY, anchor=NW)
		payrollButton.place(x=col2X, y=dbY, anchor=NW)
		searchButton.place(x=col3X, y=dbY, anchor=NW)
		if showEmpMenu:
			empMenu()
		if showAdminMenu:
			adminMenu()

def adminMenu():
	global showAdminMenu, showEmpMenu, showDBMenu
	global settingsButton, aboutButton, exitButton
	if showAdminMenu:
		showAdminMenu = False
		settingsButton.place_forget()
		aboutButton.place_forget()
		exitButton.place_forget()
	else:
		showAdminMenu = True
		settingsButton.place(x=col1X, y=admY, anchor=NW)
		aboutButton.place(x=col2X, y=admY, anchor=NW)
		exitButton.place(x=col3X, y=admY, anchor=NW)
		if showEmpMenu:
			empMenu()
		if showDBMenu:
			dbmenu()



## Interactive Creations
employeeButton = Button(root, image=UIemployee, command=empMenu)
employeeButton.place(x=1, y=empY, anchor=NW)
## Submenu Button Instantiation
timeclockButton = Button(root, image=UItimeclock, command=getTC)
laborButton = Button(root, image=UIlabor, command=getCurrentLabor)
addEmpButton = Button(root, image=UIadd, command=addNew)


dbaseButton = Button(root, image=UIdbase, command=dbmenu)
dbaseButton.place(x=1, y=dbY, anchor=NW)
## Submenu Button Instantiation
editButton = Button(root, image=UIedit, command=None)
payrollButton = Button(root, image=UIpayroll, command=None)
searchButton = Button(root, image=UIsearch, command=None)


adminButton = Button(root, image=UIadmin, command=adminMenu)
adminButton.place(x=1, y=admY, anchor=NW)
## Submenu Button Instantiation
settingsButton = Button(root, image=UIsettings, command=None)
aboutButton = Button(root, image=UIAbout, command=getAbout)
exitButton = Button(root, image=UIexit, command=killMe)



## Live Clock
time1 = None
def tick():
	global time1, clockDisplay
	time1 = " "
	time2 = time.strftime('%H:%M:%S')
	if time2 != time1:
		time1 = time2
		clockDisplay.config(text=time2)
	clockDisplay.after(1000, tick)


clockDisplay = Label(root, font="Helvetica 30 bold", bg="blue", fg="White", width=8, justify=CENTER)
clockDisplay.place(x=5, y=8, anchor=NW)

## Live Calendar
date1 = None
def cal():
	global date1
	date1 = " "
	monthJ = None
	date2 = datetime.datetime.now()
	if date2 != date1:
		date1 = date2
		dateDisplay.config(text=date2.strftime('%b %d, %Y (%a)'))
	dateDisplay.after(1000, cal)


dateDisplay = Label(root, font="Helvetica 12 bold", bg="blue", fg="white", width=14, justify=CENTER)
dateDisplay.place(x=24, y=48, anchor=NW)

def clockedInCount():
	global clockedIn
	c.execute("SELECT Count(*) FROM TimeClock WHERE isClockedIn = 1")
	data = c.fetchone()[0]
	laborRate = [0]
	laborRate.append(c.execute("SELECT EmpPay FROM TimeClock WHERE isClockedIn = 1").fetchall())
	wages = [0]
	for i in range(data):
		wages.append(sum(laborRate[1][i]))

	payOut = "¥{:,}".format(sum(wages))+" per hour"
	statusLabel.config(text="Clocked In: " + str(data) + "   Labor Cost: "+payOut)
	dateDisplay.after(900, clockedInCount)


##run the clocks
tick()
cal()

titleCard = Label(root, text="TimeLord\nby Mitakasoft", font="Helvetica 14 bold", bg="blue", fg="white", width=10, justify=RIGHT)
titleCard.place(x=430, y=15, anchor=NW)

statusLabel = Label(root, font="Helvetica 8", bg="light gray", width=35, justify=LEFT)
statusLabel.place(x=9, y=385, anchor=NW)

copyrightLabel = Label(root, text="©2019 Mitakasoft.  Made in Japan", font="Helvetica 8", bg="light gray", width=30, justify=RIGHT) .place(x=380, y=385, anchor=NW)

clockedInCount()

##runs the program
root.mainloop()