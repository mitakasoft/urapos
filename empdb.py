import sqlite3
from tkinter import *
from tkinter import messagebox
import sys
import os

##Converting Dictionary Entries into DB Entries

root = Tk()
root.title("Time Clock - Employee Entry")
root.resizable(width=False, height=False)
myColor = '#253340'
root.configure(background=myColor)

universal = "none 12 bold"

conn = sqlite3.connect('TestDB.db')
c = conn.cursor()
c.execute("CREATE TABLE IF NOT EXISTS EmpTable(EmpID INTEGER, EmpLst TEXT, EmpFst TEXT, EmpYOB INTEGER, EmpMOB INTEGER, EmpDOB INTEGER, EmpPho TEXT, EmpCou TEXT, EmpVsa TEXT, EmpPos TEXT, EmpPay INTEGER, EmpSts TEXT)")

def killMe():
	conn.close()
	exit()

def resetAll():
	empNoEntry.delete(0,END)
	lastEntry.delete(0,END)
	firstEntry.delete(0,END)
	yearEntry.delete(0,END)
	monthEntry.delete(0,END)
	dayEntry.delete(0,END)
	phoneEntry.delete(0,END)
	countryEntry.delete(0,END)
	visaEntry.delete(0,END)
	posEntry.delete(0,END)
	wageEntry.delete(0,END)
	statusEntry.delete(0,END)

	
def registerEntry():
	global empList
	try:
		empNo = int(empNoEntry.get())
		if empNo < 1000 or empNo > 9999:
			messagebox.showerror("Entry Error", "Invalid entry.\nEmployee number must start at 1001.\nCannot be greater than 9999.\nClick OK to try again.")
			resetAll()
		else:
			lastName = lastEntry.get()
			firstName = firstEntry.get()
			yearOB = int(yearEntry.get())
			monthOB = int(monthEntry.get())
			if monthOB < 1 or monthOB > 12:
				messagebox.showerror("Calendar Error", "There are only 12 months in a year and no month 0.")
				resetAll()
			dayOB = int(dayEntry.get())
			if dayOB < 1 or dayOB > 31:
				messagebox.showerror("Calendar Error", "30 Days has September, April, June and November.\nFebruary may have less.\nNever will there be a day 0 or more than 31 days.")
				resetAll()
			phone = phoneEntry.get()
			country = countryEntry.get()
			visa = visaEntry.get()
			pos = posEntry.get()
			wage = int(wageEntry.get())
			status = statusEntry.get()

			if messagebox.askyesno("Ready to Register", "Would you like to register this employee?"):
				c.execute("INSERT INTO EmpTable(EmpID, EmpLst, EmpFst, EmpYOB, EmpMOB, EmpDOB, EmpPho, EmpCou, EmpVsa, EmpPos, EmpPay, EmpSts) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
					(empNo, lastName, firstName, yearOB, monthOB, dayOB, phone, country, visa, pos, wage, status))
				conn.commit()
				messagebox.showinfo("Test Most Success", lastName + " " + firstName + " has been successfully registered to the system.")
			else:
				messagebox.showwarning("Aborted", "Submission aborted.")

			resetAll()

			

	except ValueError:
		messagebox.showinfo("Error", "Invalid or missing information.\nPlease recheck the form and try again.")






##Labels and Buttons
titleLabel = Label(root, text="Employee Entry Form", fg="dark blue", font="none 15 italic") .grid(column=1, row=1, columnspan=10, rowspan=2, sticky=W)
empNoLabel = Label(root, text="Employee Number (● ● ● ●)", font=universal) .grid(column=2, row=4, sticky=E)
lastLabel = Label(root, text="Last Name", font=universal) .grid(column=2, row=5, sticky=E)
firstLabel = Label(root, text="First Name", font=universal) .grid(column=2, row=7, sticky=E)
DOBLabel = Label(root, text="Date of Birth: Year (YYYY)", font=universal) .grid(column=2, row=9, sticky=E)
##Year Entry goes next to it
monthLabel = Label(root, text="Month (MM)", font=universal) .grid(column=2, row=11, sticky=E)
##Month entry goes next to it
dayLabel = Label(root, text="Day (DD)", font=universal) .grid(column=2, row=13, sticky=E)
##Day Entry goes next to it
phoneLabel = Label(root, text="Contact Phone", font=universal) .grid(column=2, row=15, sticky=E)
countryLabel = Label(root, text="Country (Foreigners Only)", font=universal) .grid(column=2, row=17, sticky=E)
visaLabel = Label(root, text="Visa Status (Foreigners Only)", font=universal) .grid(column=2, row=19, sticky=E)
posLabel = Label(root, text="Position", font=universal) .grid(column=2, row=21, sticky=E)
wageLabel = Label(root, text="Hourly Wage", font=universal) .grid(column=2, row=23, sticky=E)
statusLabel = Label(root, text="Status", font=universal) .grid(column=2, row=25, sticky=E)

resetAllButton= Button(root, text="Reset All", font=universal, command=resetAll) .grid(column=2, row=29, sticky=E)
cancelButton = Button(root, text="Cancel Entry", font=universal, fg="red", command=killMe) .grid(column=2, row=31, sticky=E)

registerButton = Button(root, text="Register Employee", font=universal, fg="dark green", command=registerEntry) .grid(column=3, row=29, columnspan=10, sticky=W)

instructionLabel = Label(root, text='For Japanese, enter "JAPAN" for Country and "J-NATIONAL" for Visa', fg="red", font=universal) .grid(column=1, row=32, columnspan=10, sticky=W)
instructionJapanese = Label(root, text="日本人の社員なら、Countryの所に「日本」で、Visaの所で「日本国籍」を入力して下さい", fg="red", font=universal) .grid(column=1, row=34, columnspan=10, sticky=W)

##Entry Fields
empNoEntry = Entry(root, font=universal) 
empNoEntry.grid(column=3, row=4, sticky=W, columnspan=10)
lastEntry = Entry(root, font=universal) 
lastEntry.grid(column=3, row=5, sticky=W, columnspan=10)
firstEntry = Entry(root, font=universal) 
firstEntry.grid(column=3, row=7, sticky=W, columnspan=10)
yearEntry = Entry(root, font=universal) 
yearEntry.grid(column=3, row=9, sticky=W, columnspan=10)
monthEntry = Entry(root, font=universal) 
monthEntry.grid(column=3, row=11, sticky=W, columnspan=10)
dayEntry = Entry(root, font=universal) 
dayEntry.grid(column=3, row=13, sticky=W, columnspan=10)
phoneEntry = Entry(root, font=universal) 
phoneEntry.grid(column=3, row=15, sticky=W, columnspan=10)
countryEntry = Entry(root, font=universal, fg="blue") 
countryEntry.grid(column=3, row=17, sticky=W, columnspan=10)
visaEntry = Entry(root, font=universal, fg="blue") 
visaEntry.grid(column=3, row=19, sticky=W, columnspan=10)
posEntry = Entry(root, font=universal) 
posEntry.grid(column=3, row=21, sticky=W, columnspan=10)
wageEntry = Entry(root, font=universal, fg="dark green") 
wageEntry.grid(column=3, row=23, sticky=W, columnspan=10)
statusEntry = Entry(root, font=universal) 
statusEntry.grid(column=3, row=25, sticky=W, columnspan=10)


root.mainloop()