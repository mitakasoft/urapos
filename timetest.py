import sys

def getHoursSameDay(start, finish):
	return finish - start

def getOvernightHours(start, finish):
	midTime = 2400 - start
	return midTime + finish

print(str(getHoursSameDay(830, 1730)/100) + " Hours")
print(str(getOvernightHours(2330, 2217)/100) + " Hours")